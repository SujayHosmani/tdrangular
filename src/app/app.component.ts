import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from './chat/chat.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'TDR';

  constructor(
    public router: Router,
    public chatService: ChatService,
  ) { }
  ngOnInit(): void {
    // this.router.navigateByUrl('main');
  }

  

  

}
