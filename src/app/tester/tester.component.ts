import { AppSettings, AppConstants } from '../Helpers/AppConstants';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit, AfterViewInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent, MatChip } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { IssueMainModel, RegisterModel, TaskDescFilter, ImgModel, SendImgModel, IssueMainModelConvertor, TaskModelV01, FilterModelV01, PaginationModel } from '../Model/register-model';
import { NetworkService } from '../Helpers/NetworService';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogOverviewExampleDialog } from '../Dialog/dialog-overview-example-dialog';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-tester',
  templateUrl: './tester.component.html',
  styleUrls: ['./tester.component.css']
})
export class TesterComponent implements OnInit {
  @Input() inputArray: RegisterModel[];
  @Input() inputProjName: string;

  isDisableLeft: boolean = false;
  isDisableRight: boolean = false;


  isProgressLeft: boolean = false;
  isProgressRight: boolean = false;

  isResolvedLeft: boolean = false;
  isResolvedRight: boolean = false;


  isClosedLeft: boolean = false;
  isClosedRight: boolean = false;

  openPageSlide: string;
  progressPageSlide: string;
  resolvedPageSlide: string;
  closedPageSlide: string;

  openPageNo: number = 1;
  progressPageNo: number = 1;
  resolvedPageNo: number = 1;
  closedPageNo: number = 1;

  paginationModel: PaginationModel;

  OpenTotalPage: number = 0;
  ProgressTotalPage: number = 0;
  ResolvedTotalPage: number = 0;
  ClosedTotalPage: number = 0;

  OpenTotalCount: number = 0;
  ProgressTotalCount: number = 0;
  ResolvedTotalCount: number = 0;
  ClosedTotalCount: number = 0;

  openTaskModelV01: TaskModelV01[] = [];
  progressTaskModelV01: TaskModelV01[] = [];
  resolvedTaskModelV01: TaskModelV01[] = [];
  closedTaskModelV01: TaskModelV01[] = [];

  isCountUpdated: boolean = false;

  CountO: number = 0;
  CountP: number = 0;
  CountR: number = 0;
  CountC: number = 0;
  CountT: number = 0;

  tempModifiedDt: TaskModelV01;

  isDashboard: boolean = false;
  isActivity: boolean = false;
  isTester: boolean = true;
  isMinimized: boolean = false;
  isImgMinimized: boolean = false
  isIssueSubmitted: boolean = false;
  isRaiseIssue: boolean = false;
  isExtented: boolean = false;
  isAssigned: boolean = false;
  isRaiseImg: boolean = false;
  IssueCtrl = new FormControl();
  filteredRegUsersIssue: Observable<RegisterModel[]>;
  userChipArray: RegisterModel[] = [];
  selectable = true;
  platform1: string;
  removable = true;
  filterClass = new FilterModelV01();
  separatorKeysCodes: number[] = [ENTER, COMMA];
  allRegUsers: RegisterModel[];
  images: ImgModel[] = [];
  loading: number = 0;
  filesStatus: string = "No files Attached";
  issueModel: TaskModelV01;
  projectName: string;
  taskModelV01: TaskModelV01[] = [];
  toast: string = "Toast is here";

  SortStatusArray: any = AppConstants.SortStatusArray;
  SortPlatformArray: any = AppConstants.SortPlatformArray;
  priorityArray: any = AppConstants.PriorityArray;
  typeArray: any = AppConstants.TypeArray;
  SortPriorityArray: any = AppConstants.SortPriorityArray;
  SortTypeArray: any = AppConstants.SortTypeArray;

  desid: any = AppConstants.designations;
  UserName: string = localStorage.getItem(AppConstants.local.usrName);
  UserId: string = localStorage.getItem(AppConstants.local.usrId);
  UserDes: string = localStorage.getItem(AppConstants.local.useDes);


  @ViewChild('IssueInput') IssueInput: ElementRef<HTMLInputElement>;
  @ViewChild('autoIssue') matAutocompleteIssue: MatAutocomplete;
  @ViewChild('ImgInput') ImgInputVariable: ElementRef;

  constructor(public router: Router, public networkService: NetworkService, public dialog: MatDialog) {
    this.onCreate();
  }


  async onCreate() {
    console.log("On create");
    this.removeFirstElement()
    this.filteredRegUsersIssue = this.IssueCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: RegisterModel | null) => fruit ? this._filter(fruit) : this.allRegUsers.slice()));
    this.issueModel = new TaskModelV01();
    this.filterClass = new FilterModelV01();
    this.filterClass.TaskById = this.UserId;
    this.getPaginationTasks("all", "create", 1);
  }

  ngOnInit(): void {
    this.allRegUsers = this.inputArray;
    this.projectName = this.inputProjName;
    if (!this.allRegUsers) {
      this.projectName = localStorage.getItem('projname');
      this.getUsers();
    }
  }

  async getUsers() {
    let Users = await this.networkService.performRequest(AppConstants.method.get, AppConstants.getAllUsers, null);
    if (Users[AppConstants.res.status] == 1) {
      let reg = new RegisterModel();
      reg.UserId = 'All';
      reg.UserName = 'All';
      this.allRegUsers = Users[AppConstants.res.data];
      this.allRegUsers.unshift(reg);
    }
  }


  // async getTasks() {
  //   this.removeAllFields();
  //   let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.filterIssuesV01, this.filterClass);
  //   if (response[AppConstants.res.status] == 1) {
  //     this.taskModelV01 = response[AppConstants.res.data];
  //     this.setToAll();
  //   } else {
  //     this.setToAll();

  //   }
  // }

  removeAllFields() {
    if (this.filterClass.Status == 'All') {
      this.filterClass.Status = undefined;
    }
    if (this.filterClass.TaskType == 'All') {
      this.filterClass.TaskType = undefined;
    }
    if (this.filterClass.TaskToId == 'All') {
      this.filterClass.TaskToId = undefined;
    }
    if (this.filterClass.Platform == 'All') {
      this.filterClass.Platform = undefined;
    }
    if (this.filterClass.Priority == 'All') {
      this.filterClass.Priority = undefined;
    }

  }

  setToAll() {
    if (this.filterClass.Status == undefined) {
      this.filterClass.Status = 'All';
    }
    if (this.filterClass.TaskType == undefined) {
      this.filterClass.TaskType = 'All';
    }
    if (this.filterClass.TaskToId == undefined) {
      this.filterClass.TaskToId = 'All';
    }
    if (this.filterClass.Platform == undefined) {
      this.filterClass.Platform = 'All';
    }
    if (this.filterClass.Priority == undefined) {
      this.filterClass.Priority = 'All';
    }
  }



  showPage(from: string) {
    if (from == 'dashboard') {
      this.isDashboard = true;
      this.isActivity = false;
      this.isTester = false;
    } else if (from == 'activity') {
      this.isDashboard = false;
      this.isActivity = true;
      this.isTester = false;
    } else if (from == 'tester') {
      this.isDashboard = false;
      this.isActivity = false;
      this.isTester = true;
    }
  }

  onSucessClosedClicked() {
    this.isIssueSubmitted = false;
  }

  getDesignation(des: string) {
    return AppSettings.getDesignationValue(des);
  }

  onMinimizedClicked() {
    if (this.isMinimized) {
      this.isMinimized = false;
    } else {
      this.isMinimized = true;
    }
  }

  onExtendedClicked() {
    if (this.isExtented) {
      this.isExtented = false;
    } else {
      this.isExtented = true;
    }
  }

  attachImages() {
    this.isRaiseImg = true;
    this.isImgMinimized = false;
  }


  onIntoClicked() {
    this.onRaiseClicked()
  }


  isCurrentUser(user: RegisterModel) {
    if (this.UserDes == 'admin') {
      if (this.UserId == user.UserId || user.UserId == 'All') {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.UserId == user.UserId || user.UserId == 'All' || user.UserDesignation == 'projecthead' || user.UserDesignation == 'admin') {
        return true;
      } else {
        return false;
      }
    }
  }

  onIntoImgClicked() {
    this.onRaiseImgClicked();
  }

  onImgMinimizedClicked() {
    if (this.isImgMinimized) {
      this.isImgMinimized = false;
    } else {
      this.isImgMinimized = true;
    }
  }

  onRaiseClicked() {
    this.isIssueSubmitted = false;
    this.isMinimized = false;
    if (this.isRaiseIssue) {
      this.isRaiseIssue = false;
      this.isRaiseImg = false;
    } else {
      this.isRaiseIssue = true;
    }
  }

  onRaiseImgClicked() {
    this.isImgMinimized = false;
    if (this.isRaiseImg) {
      this.isRaiseImg = false;
    } else {
      this.isRaiseImg = true;
    }
  }

  onFileChange(event) {

    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event2: any) => {
          let Img = new ImgModel();
          Img.ImageBase64 = event2.target.result
          Img.ImageFileName = event.target.files[i].name
          Img.isUploading = true;
          Img.ImageType = event.target.files[i].type;
          this.images.push(Img);
          this.submitAllImages(this.images[this.images.length - 1], filesAmount, i + 1);
        }
        reader.readAsDataURL(event.target.files[i]);
      }


    }
  }

  async submitAllImages(img: ImgModel, total: number, current: number) {
    console.log("The total ", total, " Current ", current);
    let sendImg = new SendImgModel();
    sendImg.Img = img.ImageBase64;
    sendImg.Type = this.UserId + "_" + img.ImageFileName;

    img.isUploading = true;
    img.isUploadFailed = false;
    img.isUploaded = false

    this.getFileStatus();
    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.sendImage, sendImg);
    if (response[AppConstants.res.status] == 1) {
      if (total == current) {
        if (this.ImgInputVariable) {
          if (this.ImgInputVariable.nativeElement) {
            this.ImgInputVariable.nativeElement.value = "";
          }
        }
      }
      img.isUploading = false;
      img.isUploaded = true
      img.receivedDocid = response[AppConstants.res.data];
      this.getFileStatus();
    } else {
      if (total == current) {
        if (this.ImgInputVariable) {
          if (this.ImgInputVariable.nativeElement) {
            this.ImgInputVariable.nativeElement.value = "";
          }
        }
      }
      img.isUploadFailed = true;
      img.isUploading = false;
      img.isUploaded = false
      this.getFileStatus();

    }
    this.getFileStatus();

  }

  getFileStatus() {
    var sucess = 0;
    var fail = 0;
    this.loading = 0;
    var status = "";
    for (var i = 0; i < this.images.length; i++) {
      if (this.images[i].isUploaded) {
        sucess = sucess + 1
      } else if (this.images[i].isUploadFailed) {
        fail = fail + 1;
      } else if (this.images[i].isUploading) {
        this.loading = this.loading + 1;
      }

    }

    status = ""
    if (sucess > 0) {
      status = "Files uploaded : " + sucess;
    }

    if (fail > 0) {
      status = status + ",   Failed files : " + fail;
    }

    if (this.loading > 0) {
      status = status + ",   Uploading files : " + this.loading;
    }

    if (sucess == 0 && fail == 0 && this.loading == 0) {
      this.filesStatus = "No files Attached";
    } else {
      this.filesStatus = status;
    }


  }

  checkForExistence(name: string): boolean {
    let val = name.toLowerCase().trim()
    return this.allRegUsers.some(r => r.UserName.toLowerCase().trim() === val);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      if (this.userChipArray.length == 0) {
        if (this.checkForExistence(value)) {
          let val = this.allRegUsers.filter(book => book.UserName.toLowerCase().trim() === value.toLowerCase().trim());
          this.userChipArray.push(val[0]);
          this.isAssigned = true;
        }
      }
    }

    if (input) {
      input.value = '';
    }

    this.IssueCtrl.setValue(null);
  }

  remove(fruit: RegisterModel): void {
    const index = this.userChipArray.indexOf(fruit);

    if (index >= 0) {
      this.userChipArray.splice(index, 1);
      this.isAssigned = false;
    }

    if (this.platform1) {
      if (this.platform1 && fruit.Password != 'new')
        this.onClickPlatform1(this.platform1);
    }

  }

  removeFirstElement() {
    const index = this.userChipArray.indexOf(this.userChipArray[0]);
    if (index >= 0) {
      this.userChipArray.splice(index, 1);
      this.isAssigned = false;
    }
  }


  isReportedAndClosedSame(taskV01: TaskModelV01) {
    if (taskV01.TaskById == taskV01.DevId) {
      return true;
    }
  }

  onClickPlatform1(id: string) {
    this.platform1 = id;
    this.issueModel.Platform = id;
    if (this.userChipArray.length > 0) {
      if (this.userChipArray[0].Password == 'new') {
        this.removeFirstElement();
      }
    }
    if (this.userChipArray.length == 0) {
      var regNew = new RegisterModel();
      regNew.Password = 'new'
      regNew.UserId = id
      this.isAssigned = true;
      switch (id) {
        case 'web': {
          regNew.UserName = 'WEB Team'
          this.userChipArray.push(regNew);
          break;
        }
        case 'api': {
          regNew.UserName = 'API Team'
          this.userChipArray.push(regNew)
          break;
        }
        case 'android': {
          regNew.UserName = 'Android Team'
          this.userChipArray.push(regNew)
          break;
        }
        case 'ios': {
          regNew.UserName = 'iOS Team'
          this.userChipArray.push(regNew)
          break;
        }
      }
    }

  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.userChipArray.length == 0) {
      this.userChipArray.push(event.option.value);
      this.isAssigned = true;
      this.IssueInput.nativeElement.value = '';
      this.IssueCtrl.setValue(null);
    }
    this.IssueInput.nativeElement.value = '';
    this.IssueCtrl.setValue(null);

  }

  isTeam(taskV01: TaskModelV01) {
    if (taskV01.TaskToName.includes('Team')) {
      return true;
    }
  }

  async onIssueSubmit() {
    this.issueModel.ProjName = "EInvoice";
    this.issueModel.Status = 'open'
    this.issueModel.CreatedDt = new Date();
    this.issueModel.ModifiedDt = new Date();
    this.isIssueSubmitted = false;
    this.issueModel.TaskById = this.UserId;
    this.issueModel.TaskByName = this.UserName;
    this.issueModel.MovedById = this.UserId;
    this.issueModel.MovedBy = this.UserName;
    if (this.userChipArray.length > 0) {
      this.issueModel.TaskToId = this.userChipArray[0].UserId;
      this.issueModel.TaskToName = this.userChipArray[0].UserName;
    } else {
      if (this.platform1) {
        this.onClickPlatform1(this.platform1);
        if (this.userChipArray.length > 0) {
          this.issueModel.TaskToId = this.userChipArray[0].UserId;
          this.issueModel.TaskToName = this.userChipArray[0].UserName;
        }
      }
    }
    this.issueModel.ImgArray = new Array();
    for (var i = 0; i < this.images.length; i++) {
      console.log("The docs are ", this.images[i].receivedDocid);
      if (this.images[i].isUploaded && this.images[i].receivedDocid) {
        this.issueModel.ImgArray.push(this.images[i].receivedDocid.toString());
      }
    }
    console.log(this.issueModel);

    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.postIssueV01, this.issueModel);
    if (response[AppConstants.res.status] == 1) {
      this.issueModel = new TaskModelV01();
      this.removeFirstElement();
      this.filesStatus = "No files Attached";
      this.images = [];
      this.platform1 = undefined;
      setTimeout(() => {
        this.onIntoClicked();
      }, 400);

      this.removeAllFields();
      let responseTasks = await this.networkService.performRequest(AppConstants.method.post, AppConstants.filterIssuesV01, this.filterClass);
      if (responseTasks[AppConstants.res.status] == 1) {
        this.setToAll();
        let res2 = responseTasks[AppConstants.res.data];
        setTimeout(() => {
          this.taskModelV01 = [];

        }, 700);
        setTimeout(() => {
          this.isIssueSubmitted = true;
          this.toast = "Issue Submitted Successfully"
        }, 800);
        setTimeout(async () => {
          this.taskModelV01 = res2;
        }, 1500);
      } else {
        this.setToAll();
      }

    }
  }

  private _filter(value: any): RegisterModel[] {
    try {
      if (value) {
        const filterValue = value.toLowerCase().trim();
        let ff = this.allRegUsers.filter(option =>
          option.UserName.toLowerCase().includes(filterValue) ||
          option.UserName.toLowerCase().includes(filterValue));
        console.log("filter ffd 1", ff.length);
        return ff
      } else {
        return [];
      }
    } catch (error) {
      return [];
    }


  }

  async onUpdate(taskV01: TaskModelV01) {
    taskV01.MovedById = this.UserId;
    taskV01.MovedBy = this.UserName;
    console.log("The post = ", JSON.stringify(taskV01));
    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.updateIssueV01, taskV01);
    if (response[AppConstants.res.status] == 1) {
      this.isCountUpdated = false;
      this.openPageNo = 1;
      this.progressPageNo = 1;
      this.resolvedPageNo = 1;
      this.closedPageNo = 1;
      this.getPaginationTasks("all", "create", 1);
      return true;
    } else {
      return false;
    }
  }

  async openDialogProgress(task: TaskModelV01, event: CdkDragDrop<string[]>, remark: string, description: string, status: string, title: string) {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '27%',
      minWidth: 300,
      data: { name: task.TaskDesc, remark: remark, title: title, description: description }
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed ', result);
      if (result) {
        if (description == "Resolved -> Closed") {
          this.onResolvedToClosedDrop(task, event, status, result);
        } else if (description == "Open -> Closed") {
          this.onOpenToClosedDrop(task, event, status, result);
        } else if (description == "Closed -> Open") {
          console.log("closed to open")
          this.ClosedToOpenDrop(task, event, status, result);
        } else if (description == "Closed -> Resolved") {
          this.ClosedToResolvedDrop(task, event, status, result);
        }

      } else {
        task.Status = status;
        task = this.tempModifiedDt;
        transferArrayItem(event.container.data,
          event.previousContainer.data,
          event.currentIndex,
          event.previousIndex);
      }
    });
  }

  async onResolvedToClosedDrop(TaskNewObj: TaskModelV01, event: CdkDragDrop<string[]>, status: string, remark: string) {

    if (TaskNewObj.TaskById == this.UserId) {
      TaskNewObj.ModifiedDt = new Date();
      TaskNewObj.From = "resolved"
      TaskNewObj.Flow = 'F'
      TaskNewObj.Status = "closed"
      TaskNewObj.TestRemark = remark;
      transferArrayItem(event.container.data,
        event.previousContainer.data,
        event.currentIndex,
        event.previousIndex);
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      let res = await this.onUpdate(TaskNewObj);
      if (res == true) {
        TaskNewObj.Status = "closed";
        // this.SortTaskArray();
      } else {
        TaskNewObj.Status = status;
        TaskNewObj = this.tempModifiedDt;
        transferArrayItem(event.container.data,
          event.previousContainer.data,
          event.currentIndex,
          event.previousIndex);
      }
    }


  }

  async onOpenToClosedDrop(TaskNewObj: TaskModelV01, event: CdkDragDrop<string[]>, status: string, remark: string) {

    if (TaskNewObj.TaskById == this.UserId) {
      TaskNewObj.DevId = this.UserId;
      TaskNewObj.DevName = this.UserName;
      TaskNewObj.ModifiedDt = new Date();
      TaskNewObj.From = "open"
      TaskNewObj.Flow = 'F'
      TaskNewObj.Status = "closed"
      TaskNewObj.TestRemark = remark;
      transferArrayItem(event.container.data,
        event.previousContainer.data,
        event.currentIndex,
        event.previousIndex);
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      let res = await this.onUpdate(TaskNewObj);
      if (res == true) {
        TaskNewObj.Status = "closed";
        // this.SortTaskArray();
      } else {
        TaskNewObj.Status = status;
        TaskNewObj = this.tempModifiedDt;
        transferArrayItem(event.container.data,
          event.previousContainer.data,
          event.currentIndex,
          event.previousIndex);
      }
    }


  }

  async ClosedToOpenDrop(TaskNewObj: TaskModelV01, event: CdkDragDrop<string[]>, status: string, remark: string) {
    if (TaskNewObj.DevId == this.UserId) {
      console.log("closed to open")
      TaskNewObj.DevId = null;
      TaskNewObj.DevName = null;
      TaskNewObj.ModifiedDt = new Date(); //TaskNewObj.CreatedDt;
      TaskNewObj.From = "closed"
      TaskNewObj.Flow = 'B'
      TaskNewObj.Status = "open"
      TaskNewObj.TestRemark = remark;
      transferArrayItem(event.container.data,
        event.previousContainer.data,
        event.currentIndex,
        event.previousIndex);
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      let res = await this.onUpdate(TaskNewObj);
      if (res == true) {
        TaskNewObj.Status = "open";
        // this.SortTaskArray();
      } else {
        TaskNewObj.Status = status;
        TaskNewObj = this.tempModifiedDt;
        transferArrayItem(event.container.data,
          event.previousContainer.data,
          event.currentIndex,
          event.previousIndex);
      }
    }


  }

  async ClosedToResolvedDrop(TaskNewObj: TaskModelV01, event: CdkDragDrop<string[]>, status: string, remark: string) {
    if (TaskNewObj.TaskById == this.UserId && TaskNewObj.DevId != this.UserId) {
      TaskNewObj.ModifiedDt = new Date();
      TaskNewObj.From = "closed"
      TaskNewObj.Flow = 'B'
      TaskNewObj.Status = "resolved"
      TaskNewObj.TestRemark = remark;
      transferArrayItem(event.container.data,
        event.previousContainer.data,
        event.currentIndex,
        event.previousIndex);
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      let res = await this.onUpdate(TaskNewObj);
      if (res == true) {
        TaskNewObj.Status = "resolved";
        // this.SortTaskArray();
      } else {
        TaskNewObj.Status = status;
        TaskNewObj = this.tempModifiedDt;
        transferArrayItem(event.container.data,
          event.previousContainer.data,
          event.currentIndex,
          event.previousIndex);
      }
    }


  }

  async drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      if (event.container.id == 'done') {
        if (event.previousContainer.id == 'resolved') {
          let TaskNewObj = event.previousContainer.data[event.previousIndex] as unknown as TaskModelV01;
          if (TaskNewObj.TaskById == this.UserId) {
            this.tempModifiedDt = TaskNewObj;
            TaskNewObj.Status = "closed"
            TaskNewObj.ModifiedDt = new Date();
            transferArrayItem(event.previousContainer.data,
              event.container.data,
              event.previousIndex,
              event.currentIndex);

            this.openDialogProgress(TaskNewObj, event, "Verified", "Resolved -> Closed", "resolved", "Closed");
          }


        } else if (event.previousContainer.id == 'todo') {
          let TaskNewObj = event.previousContainer.data[event.previousIndex] as unknown as TaskModelV01;
          if (TaskNewObj.TaskById == this.UserId) {
            this.tempModifiedDt = TaskNewObj;
            TaskNewObj.Status = "closed"
            TaskNewObj.ModifiedDt = new Date();
            transferArrayItem(event.previousContainer.data,
              event.container.data,
              event.previousIndex,
              event.currentIndex);

            this.openDialogProgress(TaskNewObj, event, "Not an Issur or already solved", "Open -> Closed", "open", "Closed");
          }

        }

      } else if (event.container.id == 'todo') {
        let TaskNewObj = event.previousContainer.data[event.previousIndex] as unknown as TaskModelV01;
        if (TaskNewObj.DevId == this.UserId) {
          this.tempModifiedDt = TaskNewObj;
          TaskNewObj.Status = "open"
          TaskNewObj.ModifiedDt = new Date();
          transferArrayItem(event.previousContainer.data,
            event.container.data,
            event.previousIndex,
            event.currentIndex);

          this.openDialogProgress(TaskNewObj, event, "by mistake moved to closed", "Closed -> Open", "closed", "Open");
        }
      } else if (event.container.id == 'resolved') {
        let TaskNewObj = event.previousContainer.data[event.previousIndex] as unknown as TaskModelV01;
        if (TaskNewObj.TaskById == this.UserId && TaskNewObj.DevId != this.UserId) {
          this.tempModifiedDt = TaskNewObj;
          TaskNewObj.Status = "resolved"
          TaskNewObj.ModifiedDt = new Date();
          transferArrayItem(event.previousContainer.data,
            event.container.data,
            event.previousIndex,
            event.currentIndex);

          this.openDialogProgress(TaskNewObj, event, "by mistake moved to closed", "Closed -> Resolved", "closed", "Resolved");
        }
      }

    }
  }

  onSelectChoosed(value: string) {
    this.openPageNo = 1;
    this.progressPageNo = 1;
    this.resolvedPageNo = 1;
    this.closedPageNo = 1;
    this.getPaginationTasks("all", "select", 1);
  }

  onMoreClicked(taskV01: TaskModelV01) {
    // focusMeChat34(40);
    // focusMeChat34(20);
    // localStorage.setItem(AppConstants.local.chatItem,JSON.stringify(taskV01));
    localStorage.setItem(AppConstants.local.chatItem, taskV01.TaskId);
    this.router.navigate([AppConstants.navigate.chat], { state: { taskModelV01: taskV01 } });
  }

  getPageSlide(fromPN: number, fromTotal: number) {
    let max = (fromPN * this.filterClass.pageSize)
    let ini = (max - this.filterClass.pageSize)
    if (ini == 0) {
      if (fromTotal == 0) {
        ini = 0;
      } else {
        ini = 1;
      }

    }
    if (max > fromTotal) {
      max = fromTotal;
    }
    let val = ini + "-" + max;

    return val;
  }

  async getPaginationTasks(from: string, where: string, pageNo: number) {
    this.paginationModel = new PaginationModel();
    this.removeAllFields();
    this.filterClass.from = from;
    this.filterClass.pageNumber = pageNo;
    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.filterPaginationIssuesV01, this.filterClass);
    if (response[AppConstants.res.status] == 1) {
      console.log("From Pagination =", response[AppConstants.res.data])
      if (response[AppConstants.res.data] != null) {
        this.paginationModel = response[AppConstants.res.data];
        this.calculateTotalPage();

        if (from == "all" && !this.isCountUpdated) {
          this.CountO = this.paginationModel.OpenCount;
          this.CountP = this.paginationModel.ProgressCount;
          this.CountR = this.paginationModel.ResolvedCount;
          this.CountC = this.paginationModel.ClosedCount;
          this.CountT = this.CountO + this.CountP + this.CountR + this.CountC;
          this.isCountUpdated = true;
        }

        if (from == "open" || from == "all") {
          this.OpenTotalCount = this.paginationModel.OpenCount;
          this.openTaskModelV01 = this.paginationModel.Open;
          this.openPageSlide = this.getPageSlide(this.openPageNo, this.paginationModel.OpenCount);
          this.isDisableLeft = this.disableLeft(this.openPageNo);
          this.isDisableRight = this.disableRight(this.openPageNo, this.OpenTotalPage);

        }
        if (from == "progress" || from == "all") {
          this.ProgressTotalCount = this.paginationModel.ProgressCount;
          this.progressTaskModelV01 = this.paginationModel.Progress;
          this.progressPageSlide = this.getPageSlide(this.progressPageNo, this.paginationModel.ProgressCount);
          this.isProgressLeft = this.disableLeft(this.progressPageNo);
          this.isProgressRight = this.disableRight(this.progressPageNo, this.ProgressTotalPage);

        }
        if (from == "resolved" || from == "all") {
          this.ResolvedTotalCount = this.paginationModel.ResolvedCount;
          this.resolvedTaskModelV01 = this.paginationModel.Resolved;
          this.resolvedPageSlide = this.getPageSlide(this.resolvedPageNo, this.paginationModel.ResolvedCount);
          this.isResolvedLeft = this.disableLeft(this.resolvedPageNo);
          this.isResolvedRight = this.disableRight(this.resolvedPageNo, this.ResolvedTotalPage);

        }
        if (from == "closed" || from == "all") {
          this.ClosedTotalCount = this.paginationModel.ClosedCount;
          this.closedTaskModelV01 = this.paginationModel.Closed;
          this.closedPageSlide = this.getPageSlide(this.closedPageNo, this.paginationModel.ClosedCount);
          this.isClosedLeft = this.disableLeft(this.closedPageNo);
          this.isClosedRight = this.disableRight(this.closedPageNo, this.ResolvedTotalPage);

        }
        // this.SortTaskArray();
      } else {
        this.setPageNoIfFails(from, where);
      }
    } else {
      this.setPageNoIfFails(from, where);
    }
    this.setToAll();
  }

  setPageNoIfFails(from: string, where: string) {
    switch (from) {
      case "open": {
        if (where == "next") {
          this.openPageNo = this.openPageNo - 1;
        } else if (where == "prev") {
          this.openPageNo = this.openPageNo + 1;
        }
        break;
      }
      case "progress": {
        if (where == "next") {
          this.progressPageNo = this.progressPageNo - 1;
        } else if (where == "prev") {
          this.progressPageNo = this.progressPageNo + 1;
        }
        break;
      }
      case "resolved": {
        if (where == "next") {
          this.resolvedPageNo = this.resolvedPageNo - 1;
        } else if (where == "prev") {
          this.resolvedPageNo = this.resolvedPageNo + 1;
        }
        break;
      }
      case "closed": {
        if (where == "next") {
          this.closedPageNo = this.closedPageNo - 1;
        } else if (where == "prev") {
          this.closedPageNo = this.closedPageNo + 1;
        }
        break;
      }
    }
  }


  calculateTotalPage() {
    // this.totalPage = ((this.countV01.Total + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
    this.OpenTotalPage = ((this.paginationModel.OpenCount + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
    this.ProgressTotalPage = ((this.paginationModel.ProgressCount + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
    this.ResolvedTotalPage = ((this.paginationModel.ResolvedCount + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
    this.ClosedTotalPage = ((this.paginationModel.ClosedCount + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
  }

  disableRight(fromPN: number, fromTP: number) {
    if (fromTP != undefined) {

      if (Number(fromPN) >= Number(parseInt(fromTP.toString()))) {
        return true;
      } else {
        return false;
      }
    }
  }

  disableLeft(fromPN: number) {

    if (fromPN == 1) {
      return true;
    } else {
      return false;
    }

  }

  onOpenNextClicked(from: string) {
    if (from == "open") {
      this.openPageNo = this.openPageNo + 1;
      console.log("qweqweqwev11 ", this.openPageNo, this.OpenTotalPage);
      this.getPaginationTasks('open', "next", this.openPageNo);
      // if (Number(this.openPageNo) <= Number(this.OpenTotalPage)) {
      //   console.log("qweqweqwev22 ",  this.openPageNo, this.OpenTotalPage);

      // } else {
      //   this.openPageNo = this.openPageNo - 1;
      // }
    } else if (from == "progress") {
      this.progressPageNo = this.progressPageNo + 1;
      console.log("qweqweqwev33 ", this.progressPageNo, this.ProgressTotalPage);
      this.getPaginationTasks(from, "next", this.progressPageNo);

    } else if (from == "resolved") {
      this.resolvedPageNo = this.resolvedPageNo + 1;
      this.getPaginationTasks(from, "next", this.resolvedPageNo);

    } else if (from == "closed") {
      this.closedPageNo = this.closedPageNo + 1;
      this.getPaginationTasks(from, "next", this.closedPageNo);
    }


  }

  onOpenPreviousClicked(from: string) {
    if (from == "open") {
      if (this.openPageNo != 1) {
        this.openPageNo = this.openPageNo - 1;
        this.getPaginationTasks("open", "prev", this.openPageNo);
      }
    } else if (from == "progress") {
      if (this.progressPageNo != 1) {
        this.progressPageNo = this.progressPageNo - 1;
        this.getPaginationTasks("progress", "prev", this.progressPageNo);
      }
    } else if (from == "resolved") {
      if (this.resolvedPageNo != 1) {
        this.resolvedPageNo = this.resolvedPageNo - 1;
        this.getPaginationTasks("resolved", "prev", this.resolvedPageNo);
      }
    } else if (from == "closed") {
      if (this.closedPageNo != 1) {
        this.closedPageNo = this.closedPageNo - 1;
        this.getPaginationTasks("closed", "prev", this.closedPageNo);
      }
    }


  }

}
