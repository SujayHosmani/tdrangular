import { AppSettings, AppConstants } from '../Helpers/AppConstants';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { RegisterModel, ImgModel, SendImgModel, TaskModelV01, FilterModelV01 } from '../Model/register-model';
import { NetworkService } from '../Helpers/NetworService';
import { Router } from '@angular/router';


@Component({
  selector: 'app-main-home',
  templateUrl: './main-home.component.html',
  styleUrls: ['./main-home.component.css']
})
export class MainHomeComponent {
  isDashboard: boolean = false;
  isActivity: boolean = false;
  isAnalysis: boolean = false;
  isTester: boolean = false;
  isMinimized: boolean = false;
  isImgMinimized: boolean = false
  isIssueSubmitted: boolean = false;
  isRaiseIssue: boolean = false;
  isExtented: boolean = false;
  isAssigned: boolean = false;
  isRaiseImg: boolean = false;
  CRouter:String = "All Issues";
  IssueCtrl = new FormControl();
  isDeveloper: boolean = false;
  filteredRegUsersIssue: Observable<RegisterModel[]>;
  userChipArray: RegisterModel[] = [];
  selectable = true;
  platform1: string;
  removable = true;
  filterClass = new FilterModelV01();
  separatorKeysCodes: number[] = [ENTER, COMMA];
  allRegUsers: RegisterModel[];
  images: ImgModel[] = [];
  loading: number = 0;
  filesStatus: string = "No files Attached";
  issueModel: TaskModelV01;
  projectName: string;
  isSearch: boolean = false;
  taskModelV01: TaskModelV01[] = [];
  toast: string = "Toast is here";
  mini:boolean = false;
  SortStatusArray: any = AppConstants.SortStatusArray;
  SortPlatformArray: any = AppConstants.SortPlatformArray;
  priorityArray: any = AppConstants.PriorityArray;
  typeArray: any = AppConstants.TypeArray;
  SortPriorityArray: any = AppConstants.SortPriorityArray;
  SortTypeArray: any = AppConstants.SortTypeArray;

  desid: any = AppConstants.designations;
  UserName: string = localStorage.getItem(AppConstants.local.usrName);
  UserId: string = localStorage.getItem(AppConstants.local.usrId);
  UserDes: string = localStorage.getItem(AppConstants.local.useDes);


  @ViewChild('IssueInput') IssueInput: ElementRef<HTMLInputElement>;
  @ViewChild('autoIssue') matAutocompleteIssue: MatAutocomplete;
  @ViewChild('drawer') DrawerInput: MatAutocomplete;
  @ViewChild('ImgInput') ImgInputVariable: ElementRef;

  constructor(public networkService: NetworkService, public router: Router) {
    console.log("On constructor");
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      console.log("On routeReuseStrategy");
      return false;
    }
    this.onCreate();
  }


  async onCreate() {
    console.log("On create");
    this.removeFirstElement()
    this.issueModel = new TaskModelV01();
    this.choosePage();
 
    
  }


  choosePage(){
    if (this.UserDes == "tester"){
      this.showPage('tester')
    }else if (this.UserDes == "admin" || this.UserDes == "projecthead"){
      this.showPage('dashboard')
    }else{
      this.showPage('developer')
    }
  }

  ngOnInit(): void {
    console.log("ng on init ");
      this.UserName = localStorage.getItem(AppConstants.local.usrName);
      this.UserId = localStorage.getItem(AppConstants.local.usrId);
      this.UserDes = localStorage.getItem(AppConstants.local.useDes);
      console.log(this.UserName);
      this.projectName = localStorage.getItem('projname'); 
      this.getUsers();
    
  }

  expandDrawer(){
    document.getElementById("sidddd").style.width = "250px";
      document.getElementById("mainsid").style.marginLeft = "250px";
      this.mini = false;
  }

  toggleSidebar() {
    if (this.mini) {
      console.log("opening sidebar");
      document.getElementById("sidddd").style.width = "250px";
      document.getElementById("mainsid").style.marginLeft = "250px";
      this.mini = false;
    } else {
      console.log("closing sidebar");
      document.getElementById("sidddd").style.width = "85px";
      document.getElementById("mainsid").style.marginLeft = "85px";
      
      this.mini = true;
    }
  }

  async getUsers(){
    let Users = await this.networkService.performRequest(AppConstants.method.get, AppConstants.getAllUsers, null);
    if (Users[AppConstants.res.status] == 1) {
      let reg = new RegisterModel();
      reg.UserId = 'All';
      reg.UserName = 'All';
      this.allRegUsers = Users[AppConstants.res.data];
      this.allRegUsers.unshift(reg);
      this.filteredRegUsersIssue = this.IssueCtrl.valueChanges.pipe(
        startWith(null),
        map((fruit: RegisterModel | null) => fruit ? this._filter(fruit) : this.allRegUsers.slice()));
   }
  }


 





  showPage(from:string){
    this.isRaiseIssue = false;
    this.isRaiseImg = false;
    if (from == 'dashboard'){
      this.isDashboard = true;
      this.isActivity = false;
      this.isAnalysis = false;
      this.isTester = false;
      this.isDeveloper = false;
      this.CRouter = "All Issues";
    }else if (from == 'activity'){
      this.isDashboard = false;
      this.isActivity = true;
      this.isAnalysis = false;
      this.isTester = false;
      this.isDeveloper = false;
      this.CRouter = "Activity";
    }else if (from == 'tester'){
      this.isDashboard = false;
      this.isActivity = false;
      this.isTester = true;
      this.isAnalysis = false;
      this.isDeveloper = false;
      this.CRouter = "Reported by you";
    }else if (from == 'developer'){
      this.isDashboard = false;
      this.isActivity = false;
      this.isAnalysis = false;
      this.isTester = false;
      this.isDeveloper = true;
      this.CRouter = "Assigned by you";
    }else if (from == 'analysis'){
      this.isDashboard = false;
      this.isActivity = false;
      this.isTester = false;
      this.isAnalysis = true;
      this.isDeveloper = false;
      this.CRouter = "Analysis";
    }
  }

  onSucessClosedClicked() {
    this.isIssueSubmitted = false;
  }

  getDesignation(des: string) {
    return AppSettings.getDesignationValue(des);
  }


  onSeacrchClicked(){
    if (this.isSearch){
      this.isSearch = false;
    }else{
      this.isSearch = true;
    }
   }



  onMinimizedClicked() {
    if (this.isMinimized) {
      this.isMinimized = false;
    } else {
      this.isMinimized = true;
    }
  }

  onExtendedClicked() {
    if (this.isExtented) {
      this.isExtented = false;
    } else {
      this.isExtented = true;
    }
  }

  attachImages() {
    this.isRaiseImg = true;
    this.isImgMinimized = false;
  }

  onDelete(i: any){
    this.images.splice(i, 1);
  }


  onIntoClicked() {
    this.onRaiseClicked()
  }


  isCurrentUser(user: RegisterModel) {
    if (this.UserDes == 'admin'){
      if (this.UserId == user.UserId || user.UserId == 'All'){
        return true;
      }else{
        return false;
      }
    }else{
      if (this.UserId == user.UserId || user.UserId == 'All' || user.UserDesignation == 'projecthead' || user.UserDesignation == 'admin') {
        return true;
      } else {
        return false;
      }
    }
    
  }

  onIntoImgClicked() {
    this.onRaiseImgClicked();
  }

  onImgMinimizedClicked() {
    if (this.isImgMinimized) {
      this.isImgMinimized = false;
    } else {
      this.isImgMinimized = true;
    }
  }

  onRaiseClicked() {
    this.isIssueSubmitted = false;
    this.isMinimized = false;
    if (this.isRaiseIssue) {
      this.isRaiseIssue = false;
      this.isRaiseImg = false;
    } else {
      this.isRaiseIssue = true;
    }
  }

  isTeam(taskV01: TaskModelV01){
    if (taskV01.TaskToName.includes('Team')){
      return true;
    }
  }

  onRaiseImgClicked() {
    this.isImgMinimized = false;
    if (this.isRaiseImg) {
      this.isRaiseImg = false;
    } else {
      this.isRaiseImg = true;
    }
  }

  onFileChange(event) {

    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event2: any) => {
          let Img = new ImgModel();
          Img.ImageBase64 = event2.target.result
          Img.ImageFileName = event.target.files[i].name
          Img.isUploading = true;
          Img.ImageType = event.target.files[i].type;
          this.images.push(Img);
          this.submitAllImages(this.images[this.images.length - 1], filesAmount, i + 1);
        }
        reader.readAsDataURL(event.target.files[i]);
      }


    }
  }

  async submitAllImages(img: ImgModel, total: number, current: number) {
    console.log("The total ", total, " Current ", current);
    let sendImg = new SendImgModel();
    sendImg.Img = img.ImageBase64;
    sendImg.Type = this.UserId + "_" + img.ImageFileName;

    img.isUploading = true;
    img.isUploadFailed = false;
    img.isUploaded = false

    this.getFileStatus();
    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.sendImage, sendImg);
    if (response[AppConstants.res.status] == 1) {
      if (total == current) {
        if (this.ImgInputVariable) {
          if (this.ImgInputVariable.nativeElement) {
            this.ImgInputVariable.nativeElement.value = "";
          }
        }
      }
      img.isUploading = false;
      img.isUploaded = true
      img.receivedDocid = response[AppConstants.res.data];
      this.getFileStatus();
    } else {
      if (total == current) {
        if (this.ImgInputVariable) {
          if (this.ImgInputVariable.nativeElement) {
            this.ImgInputVariable.nativeElement.value = "";
          }
        }
      }
      img.isUploadFailed = true;
      img.isUploading = false;
      img.isUploaded = false
      this.getFileStatus();

    }
    this.getFileStatus();

  }



  getFileStatus() {
    var sucess = 0;
    var fail = 0;
    this.loading = 0;
    var status = "";
    for (var i = 0; i < this.images.length; i++) {
      if (this.images[i].isUploaded) {
        sucess = sucess + 1
      } else if (this.images[i].isUploadFailed) {
        fail = fail + 1;
      } else if (this.images[i].isUploading) {
        this.loading = this.loading + 1;
      }

    }

    status = ""
    if (sucess > 0) {
      status = "Files uploaded : " + sucess;
    }

    if (fail > 0) {
      status = status + ",   Failed files : " + fail;
    }

    if (this.loading > 0) {
      status = status + ",   Uploading files : " + this.loading;
    }

    if (sucess == 0 && fail == 0 && this.loading == 0) {
      this.filesStatus = "No files Attached";
    } else {
      this.filesStatus = status;
    }


  }

  checkForExistence(name: string): boolean {
    let val = name.toLowerCase().trim()
    return this.allRegUsers.some(r => r.UserName.toLowerCase().trim() === val);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      if (this.userChipArray.length == 0) {
        if (this.checkForExistence(value)) {
          let val = this.allRegUsers.filter(book => book.UserName.toLowerCase().trim() === value.toLowerCase().trim());
          this.userChipArray.push(val[0]);
          this.isAssigned = true;
        }
      }
    }

    if (input) {
      input.value = '';
    }

    this.IssueCtrl.setValue(null);
  }

  remove(fruit: RegisterModel): void {
    const index = this.userChipArray.indexOf(fruit);

    if (index >= 0) {
      this.userChipArray.splice(index, 1);
      this.isAssigned = false;
    }

    if (this.platform1) {
      if (this.platform1 && fruit.Password != 'new')
        this.onClickPlatform1(this.platform1);
    }

  }

  removeFirstElement() {
    const index = this.userChipArray.indexOf(this.userChipArray[0]);
    if (index >= 0) {
      this.userChipArray.splice(index, 1);
      this.isAssigned = false;
    }
  }

  onClickPlatform1(id: string) {
    this.platform1 = id;
    this.issueModel.Platform = id;
    if (this.userChipArray.length > 0) {
      if (this.userChipArray[0].Password == 'new') {
        this.removeFirstElement();
      }
    }
    if (this.userChipArray.length == 0) {
      var regNew = new RegisterModel();
      regNew.Password = 'new'
      regNew.UserId = id
      this.isAssigned = true;
      switch (id) {
        case 'web': {
          regNew.UserName = 'WEB Team'
          this.userChipArray.push(regNew);
          break;
        }
        case 'api': {
          regNew.UserName = 'API Team'
          this.userChipArray.push(regNew)
          break;
        }
        case 'android': {
          regNew.UserName = 'Android Team'
          this.userChipArray.push(regNew)
          break;
        }
        case 'ios': {
          regNew.UserName = 'iOS Team'
          this.userChipArray.push(regNew)
          break;
        }
      }
    }

  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.userChipArray.length == 0) {
      this.userChipArray.push(event.option.value);
      this.isAssigned = true;
      this.IssueInput.nativeElement.value = '';
      this.IssueCtrl.setValue(null);
    }
    this.IssueInput.nativeElement.value = '';
    this.IssueCtrl.setValue(null);

  }

  async onIssueSubmit() {
    this.issueModel.ProjName = this.projectName;
    this.issueModel.Status = 'open'
    this.issueModel.CreatedDt = new Date();
    this.issueModel.ModifiedDt = new Date();
    this.isIssueSubmitted = false;
    this.issueModel.TaskById = this.UserId;
    this.issueModel.TaskByName = this.UserName;
    this.issueModel.MovedById = this.UserId;
    this.issueModel.MovedBy = this.UserName;
    if (this.userChipArray.length > 0) {
      this.issueModel.TaskToId = this.userChipArray[0].UserId;
      this.issueModel.TaskToName = this.userChipArray[0].UserName;
    } else {
      if (this.platform1) {
        this.onClickPlatform1(this.platform1);
        if (this.userChipArray.length > 0) {
          this.issueModel.TaskToId = this.userChipArray[0].UserId;
          this.issueModel.TaskToName = this.userChipArray[0].UserName;
        }
      }
    }
    this.issueModel.ImgArray = new Array();
    for (var i = 0; i < this.images.length; i++) {
      console.log("The docs are ", this.images[i].receivedDocid);
      if (this.images[i].isUploaded && this.images[i].receivedDocid) {
        this.issueModel.ImgArray.push(this.images[i].receivedDocid.toString());
      }
    }
    console.log(this.issueModel);

    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.postIssueV01, this.issueModel);
    if (response[AppConstants.res.status] == 1) {
      this.issueModel = new TaskModelV01();
      this.removeFirstElement();
      this.filesStatus = "No files Attached";
      this.images = [];
      this.platform1 = undefined;
      setTimeout(() => {
        this.isDashboard = false;
        this.onIntoClicked();
      }, 400);
      
      setTimeout(() => {
        this.taskModelV01 = [];
        this.choosePage();
      }, 700);
      setTimeout(() => {
        this.isIssueSubmitted = true;
        this.toast = "Issue Submitted Successfully"
      }, 800);
    

    }
  }

  logout() {
    if (confirm("Are you sure you want to Logout" + name)) {
      this.networkService.logOut();
      this.reloadComponent()
    }

  }

  reloadComponent() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([AppConstants.navigate.project]);
  }


  onProfileClicked(){
    this.router.navigate([AppConstants.navigate.profile]);
  }

  private _filter(value: any): RegisterModel[] {
    try {
      if (value) {
        const filterValue = value.toLowerCase().trim();
        let ff = this.allRegUsers.filter(option =>
          option.UserName.toLowerCase().includes(filterValue) ||
          option.UserName.toLowerCase().includes(filterValue));
        console.log("filter ffd 1", ff.length);
        return ff
      } else {
        return [];
      }
    } catch (error) {
      return [];
    }


  }

}
