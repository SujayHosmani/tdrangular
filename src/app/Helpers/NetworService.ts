import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AppConstants, AppSettings } from './AppConstants'
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { ErrorService } from './ErrorService';
import { SucessService } from './SucessService';
import { ProgressService } from './ProgressService';


@Injectable()
export class NetworkService {

    constructor(public router: Router, private http: HttpClient, private errorService: ErrorService,
        private sucessService: SucessService, private progressService: ProgressService) { }


        canActivate(): boolean {
            // console.log("can actiiii");
            const token = localStorage.getItem(AppConstants.local.usrId);
                if (token == null)   {
                    this.router.navigate(['login']);
                    return false;
                }
                return true;
            }

    prepareResponse(from: string, data: any) {

        if (from == "error") {
            let sendingResponse = {
                Status: 0,
                Data: null,
                Error: data
            };
            return sendingResponse;
        } else {
            let sendingResponse = {
                Status: 1,
                Data: data,
                Error: null
            };
            return sendingResponse;
        }
    }

    async performRequestAuth(usrName: string, usrPass: string) {
        this.progressService.openDialog("Loading");
        AppSettings.isBusy = true
        return await this.authenticateUser(usrName, usrPass).then((data) => {
            AppSettings.isBusy = false;
            this.progressService.closeTheDialog();
            if (data != null) {
                if (data[AppConstants.res.status] == 0){
                    this.errorService.openDialog(data['Error']);
                }
                // console.log("THE RESPONSE: ", data);
                return data;
            } else {
                this.errorService.openDialog("data is null");
                return this.prepareResponse("error", "data is null")
            }

        }).catch((err: any) => {
            this.progressService.closeTheDialog();
            // console.log("THE ERROR2: ", err);
            AppSettings.isBusy = false;
            this.errorService.openDialog(err.message);
            return this.prepareResponse("error", err.message);
        });
    }

    async performRequest(method: string, url: string, body: any) {
        if (method == AppConstants.method.get) {
            this.progressService.openDialog("Loading");
            AppSettings.isBusy = true
            return await this.httpGetMethod(url).then((data) => {
                AppSettings.isBusy = false;
                this.progressService.closeTheDialog();
                if (data != null) {
                    if (data[AppConstants.res.status] == 0){
                        this.errorService.openDialog(data['Error']);
                    }
                    // console.log("THE RESPONSE GET: ", data);
                    return data;
                }
                else {
                    this.errorService.openDialog("data is null");
                    return this.prepareResponse("error", "data is null");
                }

            }).catch((err) => {
                this.progressService.closeTheDialog();
                console.log("THE ERROR2 GET: ", err);
                AppSettings.isBusy = false;
                this.errorService.openDialog(err.message);
                return this.prepareResponse("error", err.message);
            });
        } else if (method == AppConstants.method.post) {
            if (url != AppConstants.sendImage){
                this.progressService.openDialog("Loading");
            }
            return await this.httpPostMethod(url, body).then((data) => {
                AppSettings.isBusy = false;
                this.progressService.closeTheDialog();
                if (data != null) {
                    if (data[AppConstants.res.status] == 0){
                        if (url != AppConstants.sendImage){
                            this.errorService.openDialog(data['Error']);
                        }
                    }
                    // console.log("THE RESPONSE POST: ", data);
                    return data;
                } else {
                    this.errorService.openDialog("data is null");
                    return this.prepareResponse("error", "data is null")
                }

            }).catch((err) => {
                this.progressService.closeTheDialog();
                AppSettings.isBusy = false;
                this.errorService.openDialog(err.message);
                return this.prepareResponse("error", err.message);
            });
        }

    }
// real network
    async httpGetMethod(url: string) {
        let headerss = this.getHeaders();
        try {
            let response = await this.http.get(url, { headers: headerss }).toPromise();
            if (response[AppConstants.res.status] == 1) {
                return response;
            } else if (response[AppConstants.res.status] == 0) {
                return response;
            } else {
                return this.prepareResponse("data", response);;
            }
        }
        catch (error) {
            return this.prepareResponse("error", error.message);
        }
    }

    async httpPostMethod(url: string, body: any) {
        let headerss = this.getHeaders();
        let bodyJson = JSON.stringify(body);

        // console.log("THE POST data ", bodyJson);
        try {
            let response = await this.http.post(url, bodyJson, { headers: headerss }).toPromise();
            if (response[AppConstants.res.status] == 1) {
                return response;
            } else if (response[AppConstants.res.status] == 0) {
                return response;
            } else {
                return this.prepareResponse("data", response);;
            }
        }
        catch (error) {
            return this.prepareResponse("error", error.message);
        }
    }


    async authenticateUser(usrName: string, usrPass: string) {
        let headersx = this.getHeaders();
        var data = {
            UserId: usrName,
            Password: usrPass
        };
        try {
            let response = await this.http.post(AppConstants.authenticate, data, { headers: headersx }).toPromise();
            if (response[AppConstants.res.status] == 1) {
                this.save_token("result");
                return response;
            } else if (response[AppConstants.res.status] == 0) {
                return response;
            } else {
                return this.prepareResponse("data", response);;
            }
        }
        catch (error) {
            return this.prepareResponse("error", error.message);
        }
    }


    logOut() {
        localStorage.removeItem(AppConstants.local.isLogin);
        localStorage.removeItem(AppConstants.local.useDes);
        localStorage.removeItem(AppConstants.local.usrId);
        localStorage.removeItem(AppConstants.local.usrName);
    }

    save_token(res: any) {
        // console.log("save token ", res);
        localStorage.setItem(AppConstants.local.isLogin, "true");
        return;
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // console.log("ERROR CATCH FINAL: ", error);
            return of(result as T);
        };
    }


    getHeaders() {
        var headersx: HttpHeaders;
        headersx = new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        });
        return headersx;
    }


}