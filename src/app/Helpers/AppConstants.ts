import { RegisterModel } from '../Model/register-model';

export class AppSettings {
    public static API_ENDPOINT1 = 'https://einv-apisandbox.nic.in/tdr_api/tdr/';
    public static API_ENDPOINT = 'http://10.171.23.16:5000/tdr/';
    public static isBusy = false;
    public static AllUsers: RegisterModel[] = [];

    public static getDesignationValue(key: string){
        switch(key){
            case 'tester':{
                return 'Tester';
            }
            case 'web':{
                return 'WEB Developer';
            }
            case 'api':{
                return 'API Developer';
            }
            case 'android':{
                return 'Android Developer';
            }
            case 'ios':{
                return 'iOS Developer';
            }
            case 'admin':{
                return 'Admin';
            }
            case 'projecthead':{
                return 'Project Head';
            }
        }
    }
    public static getDesignationKey(value: string){
        switch(value){
            case 'Tester':{
                return 'tester';
            }
            case 'WEB Developer':{
                return 'web';
            }
            case 'API Developer':{
                return 'api';
            }
            case 'Android Developer':{
                return 'android';
            }
            case 'iOS Developer':{
                return 'ios';
            }
            case 'Admin':{
                return 'admin';
            }
            case 'Project Head':{
                return 'projecthead';
            }
        }
    }
}
export const AppConstants = {
    authenticate: AppSettings.API_ENDPOINT + 'login',
    login: AppSettings.API_ENDPOINT + 'login',
    register: AppSettings.API_ENDPOINT + 'registration',
    weather: AppSettings.API_ENDPOINT + 'weatherforecast',
    getAllUsers: AppSettings.API_ENDPOINT + 'users',
    getImage: AppSettings.API_ENDPOINT + 'getimage/',
    getTaskById: AppSettings.API_ENDPOINT + 'task/',
    chat: AppSettings.API_ENDPOINT +  "Chat",
    filterIssues: AppSettings.API_ENDPOINT + 'taskdesc/filter',
    filterIssuesV01: AppSettings.API_ENDPOINT + 'taskdesc/filterV01',
    filterPaginationIssuesV01: AppSettings.API_ENDPOINT + 'taskdesc/pagination/filterV01',
    getCountV01: AppSettings.API_ENDPOINT + 'getCountV01',
    getworkflowbypage : AppSettings.API_ENDPOINT + 'getworkflowbypage',
    postIssue: AppSettings.API_ENDPOINT + 'taskdesc',
    postIssueV01: AppSettings.API_ENDPOINT + 'taskdescV01',
    sendImage: AppSettings.API_ENDPOINT + 'saveImage',
    updateIssue: AppSettings.API_ENDPOINT + 'updatetask',
    updateIssueV01: AppSettings.API_ENDPOINT + 'updatetaskV01',
    getIssues: AppSettings.API_ENDPOINT + 'gettaskdesc',
    getIssuesV01: AppSettings.API_ENDPOINT + 'gettaskdescV01',
    sortDevArray:['Pending to respond','In progress', 'Rejected', 'Solved', 'Un Solved','none' ],
    sortTestArray:['Pending Verification','Resolved', 'Un Resolved', 'none'],
    
    SortTypeArray: ['All','Issue','Task'],
    SortPriorityArray: ['All','High','Medium','Low','Lowest'],
    TypeArray: ['Issue','Task'],
    PriorityArray: ['High','Medium','Low','Lowest'],
    SortStatusArray: ['All','Reopened','Rejected', 'Solved', 'Unsolved'],    
    SortPlatformArray:['All','api','web','android','ios'],

    yes: 'Y',
    No: 'N',
    method: {
        get: "GET",
        post: "POST"
    },
    designations: [{
        key: "tester",
        value: "Tester"
    },{
        key: "web",
        value: "WEB Developer"
    },{
        key: "api",
        value: "API Developer"
    },{
        key: "android",
        value: "Android Developer"
    },{
        key: "ios",
        value: "iOS Developer"
    },{
        key: "projecthead",
        value: "Project Head"
    },{
        key: "admin",
        value: "Admin"
    }],

    local:{
        usrId: "userid",
        usrName: "username",
        useDes: "userdes",
        isLogin: "isLogin",
        projectName: "projname",
        chatItem: "chatitem"
    },

    res:{
        status: "Status",
        data: "Data",
        error:"Error"
    },

    navigate:{
        home:"home",
        login:"login",
        activity:"activity",
        dashboard: "dashboard",
        developer: "developer",
        tester: "tester",
        project:"project",
        chat:"chat",
        profile:"profile",
        main:'main',
        register:"register"

    }
};