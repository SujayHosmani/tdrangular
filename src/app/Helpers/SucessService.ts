import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SucessComponent } from '../sucess/sucess.component';

@Injectable()
export class SucessService {
    public isDialogOpen: Boolean = false;
    constructor(public dialog: MatDialog) { }
    openDialog(data: any): any {
        if (this.isDialogOpen) {
            return false;
        }
        this.isDialogOpen = true;
        const dialogRef = this.dialog.open(SucessComponent, {
            maxWidth: '50%',
            minWidth: '200px',
            disableClose: true,
            data: data
        });

      

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.isDialogOpen = false;
        });
    }


    closeTheDialog(){
        this.dialog.closeAll();
    }
}