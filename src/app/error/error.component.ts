import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErrorService } from '../Helpers/ErrorService';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent {
  title = 'Angular-Interceptor';
  datas:string;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private errorService: ErrorService) {
    this.datas = this.data;
  }

  closeTheDialog(){
    this.errorService.closeTheDialog();
  }
      
}
