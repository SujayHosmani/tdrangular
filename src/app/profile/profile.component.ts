import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  isDash:boolean = true;
  isSettings:boolean = false;
  isMessage:boolean = false;
  
  constructor() { }

  ngOnInit(): void {
  }

  onBackClicked(){
    
  }

  onMenuClicked(val:string){
    
   
      switch(val){
        case "dashboard":{
          console.log("va ", val)
          this.isDash = true;
          break;
        }
        case "message":{
          this.isDash = false;
          break;
         
        }
        case "settings":{
          this.isDash = false;
          break;
         
        }
      }
  }

}
