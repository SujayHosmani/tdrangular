import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './projects/projects.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ChatComponent } from './chat/chat.component';
import { MainHomeComponent } from './main-home/main-home.component';
import { DummyComponent } from './dummy/dummy.component';
import { NetworkService } from './Helpers/NetworService';
import { ActivityComponent } from './activity/activity.component';
import { TempComponent } from './temp/temp.component';
import { DeveloperComponent } from './developer/developer.component';
import { DiscussionComponent } from './discussion/discussion.component';
import { MyTaskComponent } from './my-task/my-task.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { TesterComponent } from './tester/tester.component';
import { ProfileComponent } from './profile/profile.component';
import { AnalysisComponent } from './analysis/analysis.component';


const routes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {
      path: 'main', component: MainHomeComponent,
      data: { shouldDetach: true},
      canActivate: [NetworkService]
  },
  {path: 'project', component: ProjectsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent, data: { shouldDetach: true} },
  {path: 'register', component: RegisterComponent, data: { shouldDetach: true} },
  {path: 'chat', component: ChatComponent},
  {path: 'dummy', component: DummyComponent},
  {path: 'activity', component: ActivityComponent},
  {path: 'temp', component: TempComponent},
  {path: 'dev', component: DeveloperComponent},
  {path: 'dis', component: DiscussionComponent},
  {path: 'dashboard', component: DashBoardComponent},
  {path: 'developer', component: DeveloperComponent},
  {path: 'tester', component: TesterComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'analysis', component: AnalysisComponent},
  {path: 'mytask', component: MyTaskComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
