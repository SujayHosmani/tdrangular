import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { AppConstants } from '../Helpers/AppConstants';
import { Router } from '@angular/router';
import { ChatService } from '../chat/chat.service';
import { NetworkService } from '../Helpers/NetworService';
import { ProgressService } from '../Helpers/ProgressService';
import { Message } from '../Model/Message';
import { SendImgModel, WorkFlow, WorkFlowC } from '../Model/register-model';
import { formatDate, Location } from '@angular/common';



@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @ViewChild('scrollMe2') private myScrollContainer2: ElementRef;
  @ViewChild('textArea', { read: ElementRef }) textArea: ElementRef;
  isScrolledforFirstTime: boolean = false;
  isScrolledforFirstTime2: boolean = false;
  historyEvent:any;
  messageEvent:any;
  groupName = "-999999999";
  isFirstIndex: boolean = false;
  firstIndex: any;
  isConnectionEvent:any;
  isConnectionStarted:boolean;
  changeCount: number = 0;
  changeCount2: number = 0;
  images: SendImgModel[];
  txtMessage: string;  
  messages = new Array<Message>();  
  activities = new Array<WorkFlowC>();  
  message = new Message(); 

  UserName: string = localStorage.getItem(AppConstants.local.usrName);
  UserId: string = localStorage.getItem(AppConstants.local.usrId);
  UserDes: string = localStorage.getItem(AppConstants.local.useDes);
  previousDate:Date;
  currentDate:Date;
  fff: any = [];

  constructor(private chatService: ChatService,  
    private _ngZone: NgZone  ,private progressService: ProgressService,private networkService: NetworkService,public router: Router, private _location: Location) { 
      this.subscribeToEvents(); 
    }

 

  ngOnInit(): void {
    this.scrollToBottom();
    this.getActivities();
  }
  ngAfterViewChecked(): void {
      this.scrollToBottom();
  }

  getDescription(val: WorkFlowC){
    if (val.Flow == "F"){
      if (val.From == "Init"){
        if (val.TaskType == "Issue"){
          let txt = val.TaskByName + " <strong>Reported</strong> the <strong>Issue</strong> and Assigned to <strong>" + val.TaskToName + "</strong>";
          return txt;
        }else{
          let txt = val.TaskByName + " <strong>Created</strong> the <strong>Task</strong> and Assigned to <strong>" + val.TaskToName + "</strong>";
          return txt;
        }
      }else{
        if (val.From == "open" && val.To == "progress"){
          let txt = val.DevName + " <strong>Responded</strong> to the <strong>" + val.TaskType + "</strong> which assigned to <strong>" + val.TaskToName + "</strong> and the " + val.TaskType + " currently in <strong>Progress</strong>";
          return txt;
        }else if (val.From == "progress" && val.To == "resolved"){
          let txt = val.DevName + " <strong>solved and completed</strong> the <strong>" + val.TaskType  + "</strong> and Moved to the <strong>Resoved</strong> list, further verification is <strong>pending</strong> by <strong>" + val.TaskByName + "</strong>";
          return txt;
        }else if (val.From == "resolved" && val.To == "closed"){
          let txt = val.TaskByName + " <strong>verified</strong> the <strong>" +  val.TaskType  + "</strong> that has been <strong>resolved</strong> by <strong>" + val.DevName + "</strong> it is cleard and the " + val.TaskType + " is <strong>closed</strong>";
          return txt;
        }else if (val.From == "open" && val.To == "closed"){
          let txt = val.TaskByName + " <strong>Directly closed</strong> the " + val.TaskType  + " with Remarks: " + "\n" + val.TestRemark;
          return txt;
        }
      }
    }else{
      if (val.From == "progress" && val.To == "open"){
        let txt = val.Movedby + " <strong>put back</strong> the <strong>" + val.TaskType + "</strong> from <strong>" + val.From + "</strong> to " + val.To + " with Remarks: ";
        return txt;
      }else if (val.From == "resolved" && val.To == "progress"){
        let txt = val.Movedby + " <strong>put back</strong> the <strong>" + val.TaskType + "</strong> from <strong>" + val.From + "</strong> to " + val.To + " with Remarks: ";
        return txt;
      }else if (val.From == "closed" && val.To == "open"){
        let txt = val.Movedby + " <strong>put back</strong> the <strong>" + val.TaskType + "</strong> from <strong>" + val.From + "</strong> to " + val.To + " with Remarks: ";
        return txt;
      }else if (val.From == "closed" && val.To == "resolved"){
        let txt = val.Movedby + " <strong>put back</strong> the <strong>" + val.TaskType + "</strong> from <strong>" + val.From + "</strong> to " + val.To + " with Remarks: ";
        return txt;
      }
    }
  }

  sendMessage(): void {  
    if (this.txtMessage) {  
      this.message = new Message();  
      this.message.taskId = this.groupName;  
      this.message.userName = this.UserName;  
      this.message.msg = this.txtMessage.trim();  
      this.message.userId = this.UserId;  
      this.message.date = new Date();  
      // this.messages.push(this.message);  
      this.chatService.sendMessage(this.message);  
      this.txtMessage = '';  
    }  
  } 

  ngOnDestroy(): void {
    this.chatService.leaveGroup(this.groupName);
    this.historyEvent.unsubscribe();
    this.messageEvent.unsubscribe();
  }

  async getActivities(){
    let response = await this.networkService.performRequest(AppConstants.method.get, AppConstants.getworkflowbypage + "/1/50", null);
    console.log("The response = ", response);
    if (response[AppConstants.res.status] == 1) {
     this.activities = response[AppConstants.res.data];
    }
  }

  scrollToBottom(): void {
    try {

      if (!this.isScrolledforFirstTime){
        console.log(this.changeCount , this.fff.length, this.myScrollContainer.nativeElement.scrollHeight);
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        
        if (this.fff.length > 0){
          if (this.changeCount == this.fff.length){
            this.isScrolledforFirstTime = true;
          }else{
            this.isScrolledforFirstTime = false;
          }
        }
        

        this.changeCount = this.fff.length;
      }
        
    } catch(err) { }  
    // if (this.isChat){
    //   this.isScrolledforFirstTime2 = false;
    //   this.scrollToBottom2();    
    // }
               
}

scrollToBottom2(): void {
  try {

    if (!this.isScrolledforFirstTime2){
      console.log(this.changeCount , this.fff.length, this.myScrollContainer2.nativeElement.scrollHeight);
      this.myScrollContainer2.nativeElement.scrollTop = this.myScrollContainer2.nativeElement.scrollHeight;
      
      if (this.fff.length > 0){
        if (this.changeCount2 == this.fff.length){
          this.isScrolledforFirstTime2 = true;
        }else{
          this.isScrolledforFirstTime2 = false;
        }
      }
      

      this.changeCount2 = this.fff.length;
    }
      
  } catch(err) { }                 
}

getDate(val: any){
  var ss = formatDate(val, 'hh:mm a', 'en_US')
  return ss;
}

showDateW(val: WorkFlow, index: any){
  // return true;
  if (val[index].role == "W"){
    if (this.isFirstIndex == false){
      this.isFirstIndex = true;
      this.firstIndex = index;
      return true;
    }else{
      if (index > 0){
        if (this.firstIndex == index){
          return true
        }else{
          let previousVal = formatDate(val[index - 1].createdDt, 'yyyy-MM-dd hh', 'en_US');
          let currentVal = formatDate(val[index].createdDt, 'yyyy-MM-dd hh', 'en_US');
        
          if (previousVal == currentVal){
            return false;
          }else{
            return true;
          }
        }
        

      }else{
        return true;
      }

    }

  }

}


showDate(val: WorkFlow, index: any){
  // return true;
  if (val[index].role == "M"){
    if (this.isFirstIndex == false){
      this.isFirstIndex = true;
      this.firstIndex = index;
      return true;
    }else{
      if (index > 0){
        if (this.firstIndex == index){
          return true
        }else{
          let previousVal = formatDate(val[index - 1].createdDt, 'yyyy-MM-dd hh', 'en_US');
          let currentVal = formatDate(val[index].createdDt, 'yyyy-MM-dd hh', 'en_US');
        
          if (previousVal == currentVal){
            return false;
          }else{
            return true;
          }
        }
        

      }else{
        return true;
      }

    }

  }

}

checkForDate(val: any){
  val = formatDate(val, 'dd MMM yyyy', 'en_US')
 return val;

}

 get sortData() {
  return this.fff;
}

private subscribeToEvents(): void {  
  console.log("subscribeToEvents(): ");
  //bfore this task id has to be registered
  // this.chatService.joinGroup(this.taskModelV01.TaskId);
   

  this.historyEvent = this.chatService.historyEvent.subscribe((messageArr: any) => {  
    this._ngZone.run(() => {
        console.log("history: ", messageArr);
        this.fff = messageArr; 
        // this.fff = this.sortData();
        this.isScrolledforFirstTime = false;
        this.scrollToBottom();  
      });  
     }); 

  this.messageEvent = this.chatService.messageReceived.subscribe((data: WorkFlow) => {  
    this._ngZone.run(() => {  
      console.log("messageReceived: ", data);
        
        this.fff.push(data);
        this.isScrolledforFirstTime = false;
        this.scrollToBottom();  
    });  
  });  

  this.isConnectionEvent = this.chatService.connectionEstablished.subscribe((data: boolean) => {  
    this._ngZone.run(() => {  
      console.log("The Connection here we go: ", data);
      if (this.isConnectionStarted != undefined){
        if (this.isConnectionStarted == false){
          this.chatService.joinGroup(this.groupName);
        }
      }
      

    });  
  });
}  

ngAfterViewInit(): void {
  if (this.chatService.connectionIsEstablished == true){
    this.isConnectionStarted = true;
    this.chatService.joinGroup(this.groupName);
  }else{
    this.isConnectionStarted = false;
  }
  
}

onBackClicked(){
  this._location.back();
}

onCreate(){

  this.subscribeToEvents(); 
}



}

