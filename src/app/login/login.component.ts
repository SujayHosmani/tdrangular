import { Component, OnInit } from '@angular/core';
import { RegisterModel, LoginModel } from '../Model/register-model'
import { NetworkService } from '../Helpers/NetworService';
import { AppConstants } from '../Helpers/AppConstants';
import { ErrorService } from '../Helpers/ErrorService';
import { Router } from '@angular/router';
import { SucessService } from '../Helpers/SucessService';

function hello() {
  document.querySelector('.img__btn').addEventListener('click', function () {
    document.querySelector('.cont').classList.toggle('s--signup');
  });
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  regModel: RegisterModel;
  regResModel: RegisterModel;
  loginModel: LoginModel;
  note: string = "All fields are mandatory";
  designations:any;

  constructor(
    private network: NetworkService,
    private sucessService: SucessService,
    private errorService: ErrorService,
    public router: Router
  ) {
    this.regModel = new RegisterModel();
    this.regResModel = new RegisterModel();
    this.loginModel = new LoginModel();
    this.designations = AppConstants.designations;
  }


  async onSubmitLogin(val: any) {
    console.log("on Submitted Login", val);
    if (val) {
      let login = await this.network.performRequestAuth(this.loginModel.UserId,this.loginModel.Password);
      if (login[AppConstants.res.status] == 1) {
        this.loginModel = new LoginModel();
        this.regResModel = login[AppConstants.res.data];
        localStorage.setItem(AppConstants.local.usrName, this.regResModel.UserName);
        localStorage.setItem(AppConstants.local.usrId, this.regResModel.UserId);
        await localStorage.setItem(AppConstants.local.useDes, this.regResModel.UserDesignation);
        this.reloadComponent();
       
        this.router.navigate([AppConstants.navigate.project], { state: { from: "refresh" } });
      }

    }else{
      this.errorService.openDialog(this.validateLoginForm())
    }

  }

    reloadComponent() {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([AppConstants.navigate.project]);
    }

  async onSubmitRegister(val: any) {
    console.log("on Submitted ", val);
    var UserIdz = localStorage.getItem(AppConstants.local.usrId);
    if(UserIdz.toLowerCase() == "sujayjay"){
      if (val) {
        let register = await this.network.performRequest(AppConstants.method.post, AppConstants.register, this.regModel);
        if (register[AppConstants.res.status] == 1) {
          this.regModel = new RegisterModel();
          document.querySelector('.cont').classList.toggle('s--signup');
          this.sucessService.openDialog("Registered sucessfully");
        }
  
      }else{
        
        this.errorService.openDialog(this.validateForm())
      }
    }
    

  }

  validateForm(){
    var pass = new RegExp('^[0-9A-Za-z]{4}$');
    var name = new RegExp('^[A-Z a-z]{3,30}$');
    var uid = new RegExp('^[A-Za-z0-9]{5,10}$');
    var des = new RegExp('^[A-Za-z]*$');
    var error = "";
    if (!name.test(this.regModel.UserName) || !this.regModel.UserName){
      error = 'Enter valid name \n'
    }
    if (!uid.test(this.regModel.UserId)  || !this.regModel.UserId){
      error += 'Enter valid user id \n'
    }
    if (!des.test(this.regModel.UserDesignation) || !this.regModel.UserDesignation){
      error += 'Enter valid designation \n'
    }
    if (!pass.test(this.regModel.Password)){
      error += 'Enter valid password'
    }

    return error;
  }

  validateLoginForm(){
    var pass = new RegExp('^[0-9A-Za-z]{4}$');
    var uid = new RegExp('^[A-Za-z0-9]{5,10}$');
    var error = "";
   
    if (!uid.test(this.loginModel.UserId)  || !this.loginModel.UserId){
      error += 'Enter valid user id \n'
    }
    if (!pass.test(this.loginModel.Password)){
      error += 'Enter valid password'
    }

    return error;
  }

  onTextChange(from: string) {
    switch (from) {
      case "password": {
        this.note = "Password: a-z A-Z 0-9 accepted max 4 characters " + this.regModel.Password;
        break;
      }
      case "userid": {
        this.note = "User Id: a-z A-Z max 10 characters " + this.regModel.UserId;
        break;
      }
      case "username": {
        this.note = "Username: a-z A-Z characters " + this.regModel.UserName;
        break;
      }
      case "designation": {
        this.note = "Designation: a-z A-Z characters " + this.regModel.UserDesignation;
        break;
      }
      default: {
        //statements; 
        break;
      }
    }
  }

  ngOnInit(): void {
    hello();

  }



}
