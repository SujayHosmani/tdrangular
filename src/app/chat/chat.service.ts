import { EventEmitter, Injectable } from '@angular/core';  
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';  
import { Message } from '../Model/Message';  
import { emit } from 'process';
import { WorkFlow } from '../Model/register-model';
import { AppConstants } from '../Helpers/AppConstants';
  
@Injectable()  
export class ChatService {  
  messageReceived = new EventEmitter<WorkFlow>();  
  historyEvent = new EventEmitter<WorkFlow>();  
  connectionEstablished = new EventEmitter<Boolean>();  
  
  public connectionIsEstablished = false;  
  private _hubConnection: HubConnection;  
  
  constructor() {  
    this.createConnection();  
    this.registerOnServerEvents();  
    this.startConnection();  
  }  
  
  sendMessage(message: Message) {  
    console.log("sending msg");
    this._hubConnection.invoke('SendMessage', message);  
  }  

  joinGroup(taskId: string){
    console.log("joinGroup ", taskId);
    this._hubConnection.invoke('JoinGroup',""+taskId.toString());
    
  }

  leaveGroup(taskId: string){
    console.log("Leaving Group: ", ""+taskId.toString());
    this._hubConnection.invoke('LeaveGroup',""+taskId.toString());
    
  }
  
  private createConnection() {  
    this._hubConnection = new HubConnectionBuilder()  
      .withUrl(AppConstants.chat)  
      .build();  
  }  
  
  private startConnection(): void {  
    console.log("This Is Starting Connection");
    this._hubConnection  
      .start()  
      .then(() => {  
        this.connectionIsEstablished = true;  
        console.log('Hub connection started');  
        this.connectionEstablished.emit(true);  
      })  
      .catch(err => {  
        console.log('Error while establishing connection, retrying...');  
        setTimeout(function () { this.startConnection(); }, 5000);  
      });  
  }  
  
  private registerOnServerEvents(): void {  
    this._hubConnection.on('Send', (data: any) => {  
      console.log("ChatService Receive Message");
      this.messageReceived.emit(data);  

    });
    
    this._hubConnection.on('History', (data: any) => {  
      console.log("ChatService History rec");
        this.historyEvent.emit(data);  
      });

  }  
}    