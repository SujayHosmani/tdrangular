import { Component, OnInit, AfterViewInit, NgZone, OnDestroy, ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { AppConstants } from '../Helpers/AppConstants';
import { Router } from '@angular/router';
import { formatDate, Location } from '@angular/common';
import { IssueMainModel, SendImgModel, TaskModelV01, WorkFlow } from '../Model/register-model';
import { ProgressService } from '../Helpers/ProgressService';
import { NetworkService } from '../Helpers/NetworService';
import { Message } from '../Model/Message';
import { ChatService } from './chat.service';

// function focusMeChat(id) {
//   var rowpos = $('#FocusME'+id).position();
//   console.log("row ", rowpos);
//   rowpos.top = rowpos.top;
//   $('#container').animate({ scrollTop: rowpos.top }, 600);
// };

// function ScrollToTop() {
//   document.body.scrollTop = 0;
//   document.documentElement.scrollTop = 0;
// };


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements AfterViewInit, OnDestroy, OnInit, AfterViewChecked{
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @ViewChild('scrollMe2') private myScrollContainer2: ElementRef;
  @ViewChild('textArea', { read: ElementRef }) textArea: ElementRef;
  taskModelV01:TaskModelV01;
  intentExtras:any;
  taskid:any;
  isChat: boolean = false;
  isFirstIndex: boolean = false;
  firstIndex: any;
  toggler: string = 'comments';
  isScrolledforFirstTime: boolean = false;
  isScrolledforFirstTime2: boolean = false;
  historyEvent:any;
  messageEvent:any;
  isConnectionEvent:any;
  isConnectionStarted:boolean;
  changeCount: number = 0;
  changeCount2: number = 0;
  images: SendImgModel[];
  txtMessage: string;  
  messages = new Array<Message>();  
  message = new Message(); 

  UserName: string = localStorage.getItem(AppConstants.local.usrName);
  UserId: string = localStorage.getItem(AppConstants.local.usrId);
  UserDes: string = localStorage.getItem(AppConstants.local.useDes);
  previousDate:Date;
  currentDate:Date;
  fff: any = [];

  constructor( private chatService: ChatService,  
    private _ngZone: NgZone  ,private progressService: ProgressService,private networkService: NetworkService,public router: Router, private _location: Location) { 

    try{
      // this.onCreate();
       this.intentExtras = this.router.getCurrentNavigation().extras.state.taskModelV01;
        if (this.intentExtras) {
           this.onCreate();
        } else {
          this.getFromStorage();
        }
    }catch (error) {
      // this._location.back();
      this.getFromStorage();
    }
    
  }
  ngOnInit(): void {
    this.scrollToBottom();
  }
  ngAfterViewChecked(): void {
      this.scrollToBottom();
  }

  showChatToggle(){
    this.isScrolledforFirstTime = false;
    this.scrollToBottom();  
    this.isScrolledforFirstTime2 = false;
    this.scrollToBottom2();  
    if (this.isChat){
      this.isChat = false;
    }else{
      this.isChat = true;
    }
  }

  getFromStorage(){
    var ReceivedtaskId = localStorage.getItem(AppConstants.local.chatItem);
    // this.intentExtras = JSON.parse(retrievedObject);
    this.getFromNetwork(ReceivedtaskId);
  }

  async getFromNetwork(ReceivedtaskId: any){
    let response = await this.networkService.performRequest(AppConstants.method.get, AppConstants.getTaskById + ReceivedtaskId, null);
    console.log("The response = ", response, ReceivedtaskId);
    if (response[AppConstants.res.status] == 1) {
      this.intentExtras =  response[AppConstants.res.data];
      this.onCreate();
    }
  }

  public autoGrow() {
    const textArea = this.textArea.nativeElement;
    textArea.style.overflow = 'hidden';
    textArea.style.height = '0px';
    textArea.style.height = textArea.scrollHeight + 'px';
   }

  scrollToBottom(): void {
    try {

      if (!this.isScrolledforFirstTime){
        console.log(this.changeCount , this.fff.length, this.myScrollContainer.nativeElement.scrollHeight);
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        
        if (this.fff.length > 0){
          if (this.changeCount == this.fff.length){
            this.isScrolledforFirstTime = true;
          }else{
            this.isScrolledforFirstTime = false;
          }
        }
        

        this.changeCount = this.fff.length;
      }
        
    } catch(err) { }  
    if (this.isChat){
      this.isScrolledforFirstTime2 = false;
      this.scrollToBottom2();    
    }
               
}

scrollToBottom2(): void {
  try {

    if (!this.isScrolledforFirstTime2){
      console.log(this.changeCount , this.fff.length, this.myScrollContainer2.nativeElement.scrollHeight);
      this.myScrollContainer2.nativeElement.scrollTop = this.myScrollContainer2.nativeElement.scrollHeight;
      
      if (this.fff.length > 0){
        if (this.changeCount2 == this.fff.length){
          this.isScrolledforFirstTime2 = true;
        }else{
          this.isScrolledforFirstTime2 = false;
        }
      }
      

      this.changeCount2 = this.fff.length;
    }
      
  } catch(err) { }                 
}



getDate(val: any){
  var ss = formatDate(val, 'hh:mm a', 'en_US')
  return ss;
}

showDateW(val: WorkFlow, index: any){
  // return true;
  if (val[index].role == "W"){
    if (this.isFirstIndex == false){
      this.isFirstIndex = true;
      this.firstIndex = index;
      return true;
    }else{
      if (index > 0){
        if (this.firstIndex == index){
          return true
        }else{
          let previousVal = formatDate(val[index - 1].createdDt, 'yyyy-MM-dd hh', 'en_US');
          let currentVal = formatDate(val[index].createdDt, 'yyyy-MM-dd hh', 'en_US');
        
          if (previousVal == currentVal){
            return false;
          }else{
            return true;
          }
        }
        

      }else{
        return true;
      }

    }

  }

}

getDescription(val: WorkFlow){
  if (val.flow == "F"){
    if (val.from == "Init"){
      if (val.taskType == "Issue"){
        let txt = val.taskByName + " <strong>Reported</strong> the <strong>Issue</strong> and Assigned to <strong>" + val.taskToName + "</strong>";
        return txt;
      }else{
        let txt = val.taskByName + " <strong>Created</strong> the <strong>Task</strong> and Assigned to <strong>" + val.taskToName + "</strong>";
        return txt;
      }
    }else{
      if (val.from == "open" && val.to == "progress"){
        let txt = val.devName + " <strong>Responded</strong> to the <strong>" + val.taskType + "</strong> which assigned to <strong>" + val.taskToName + "</strong> and the " + val.taskType + " currently in <strong>Progress</strong>";
        return txt;
      }else if (val.from == "progress" && val.to == "resolved"){
        let txt = val.devName + " <strong>solved and completed</strong> the <strong>" + val.taskType  + "</strong> and Moved to the <strong>Resoved</strong> list, further verification is <strong>pending</strong> by <strong>" + val.taskByName + "</strong>";
        return txt;
      }else if (val.from == "resolved" && val.to == "closed"){
        let txt = val.taskByName + " <strong>verified</strong> the <strong>" +  val.taskType  + "</strong> that has been <strong>resolved</strong> by <strong>" + val.devName + "</strong> it is cleard and the " + val.taskType + " is <strong>closed</strong>";
        return txt;
      }else if (val.from == "open" && val.to == "closed"){
        let txt = val.taskByName + " <strong>Directly closed</strong> the " + val.taskType  + " with Remarks: " + "\n" + val.testRemark;
        return txt;
      }
    }
  }else{
    if (val.from == "progress" && val.to == "open"){
      let txt = val.movedby + " <strong>put back</strong> the <strong>" + val.taskType + "</strong> from <strong>" + val.from + "</strong> to " + val.to + " with Remarks: ";
      return txt;
    }else if (val.from == "resolved" && val.to == "progress"){
      let txt = val.movedby + " <strong>put back</strong> the <strong>" + val.taskType + "</strong> from <strong>" + val.from + "</strong> to " + val.to + " with Remarks: ";
      return txt;
    }else if (val.from == "closed" && val.to == "open"){
      let txt = val.movedby + " <strong>put back</strong> the <strong>" + val.taskType + "</strong> from <strong>" + val.from + "</strong> to " + val.to + " with Remarks: ";
      return txt;
    }else if (val.from == "closed" && val.to == "resolved"){
      let txt = val.movedby + " <strong>put back</strong> the <strong>" + val.taskType + "</strong> from <strong>" + val.from + "</strong> to " + val.to + " with Remarks: ";
      return txt;
    }
  }
}

getBC(val: WorkFlow){
  if (val.flow == "F"){
    if (val.from == "Init"){
     
    }else{
     
    }
  }else{
    return "Reserve";
  }
}

showDate(val: WorkFlow, index: any){
  // return true;
  if (val[index].role == "M"){
    if (this.isFirstIndex == false){
      this.isFirstIndex = true;
      this.firstIndex = index;
      return true;
    }else{
      if (index > 0){
        if (this.firstIndex == index){
          return true
        }else{
          let previousVal = formatDate(val[index - 1].createdDt, 'yyyy-MM-dd hh', 'en_US');
          let currentVal = formatDate(val[index].createdDt, 'yyyy-MM-dd hh', 'en_US');
        
          if (previousVal == currentVal){
            return false;
          }else{
            return true;
          }
        }
        

      }else{
        return true;
      }

    }

  }

}

checkForDate(val: any){
  val = formatDate(val, 'dd MMM yyyy', 'en_US')
 return val;

}

get sortData() {
  return this.fff.sort((a, b) => {
    return <any>new Date(a.createdDt) - <any>new Date(b.createdDt);
  });
}
  

  sendMessage(): void {  
    if (this.txtMessage) {  
      this.message = new Message();  
      this.message.taskId = this.taskModelV01.TaskId.toString();  
      this.message.userName = this.UserName;  
      this.message.msg = this.txtMessage.trim();  
      this.message.userId = this.UserId;  
      this.message.date = new Date();  
      // this.messages.push(this.message);  
      this.chatService.sendMessage(this.message);  
      this.txtMessage = '';  
    }  
  }  

  


  private subscribeToEvents(): void {  
    console.log("subscribeToEvents(): ");
    //bfore this task id has to be registered
    // this.chatService.joinGroup(this.taskModelV01.TaskId);
     

    this.historyEvent = this.chatService.historyEvent.subscribe((messageArr: any) => {  
      this._ngZone.run(() => {
          console.log("history: ", messageArr);
          this.fff = messageArr; 
          // this.fff = this.sortData();
          this.isScrolledforFirstTime = false;
          this.scrollToBottom();  
        });  
       }); 

    this.messageEvent = this.chatService.messageReceived.subscribe((data: WorkFlow) => {  
      this._ngZone.run(() => {  
        console.log("messageReceived: ", data);
          
          this.fff.push(data);
          this.isScrolledforFirstTime = false;
          this.scrollToBottom();  
      });  
    });  

    this.isConnectionEvent = this.chatService.connectionEstablished.subscribe((data: boolean) => {  
      this._ngZone.run(() => {  
        console.log("The Connection here we go: ", data);
        if (this.isConnectionStarted != undefined){
          if (this.isConnectionStarted == false){
            this.chatService.joinGroup(this.taskModelV01.TaskId.toString());
          }
        }
        

      });  
    });
  }  

  ngAfterViewInit(): void {
    if (this.chatService.connectionIsEstablished == true){
      this.isConnectionStarted = true;
      this.chatService.joinGroup(this.taskModelV01.TaskId.toString());
    }else{
      this.isConnectionStarted = false;
    }
    
  }

  ngOnDestroy(): void {
    this.chatService.leaveGroup(this.taskModelV01.TaskId.toString());
    this.historyEvent.unsubscribe();
    this.messageEvent.unsubscribe();
  }

  onBackClicked(){
    this._location.back();
  }

  onCreate(){
    this.taskModelV01 = new TaskModelV01();
    this.taskModelV01 = this.intentExtras;
    this.subscribeToEvents(); 
     this.getImages(this.taskModelV01);
  }


  async getImages(task: TaskModelV01){
    this.images = [];
    console.log("The Images count", task.ImgCount);
    if (task.ImgCount){
      if (Number(task.ImgCount) > 0){
        let response = await this.networkService.performRequest(AppConstants.method.get,AppConstants.getImage + task.TaskId,null);
        console.log("The Response Images", response);
        if (response[AppConstants.res.status] == 1){
          console.log(response);
          this.images = response[AppConstants.res.data];
         
        }else{
          this.images = [];
        }
      }
    }
    
  }



}
