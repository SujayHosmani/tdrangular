import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { AppConstants, AppSettings } from '../Helpers/AppConstants';


@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.css']
})
export class DummyComponent {

  isDashboard:boolean = false;
  isActivity:boolean = true;
  desid:any = AppConstants.designations;
  UserName: string = localStorage.getItem(AppConstants.local.usrName);
  UserId: string = localStorage.getItem(AppConstants.local.usrId);
  UserDes: string = localStorage.getItem(AppConstants.local.useDes);

  constructor() { }

  ngOnInit(): void {
  }

  showPage(from:string){
    if (from == 'dashboard'){
      this.isDashboard = true;
      this.isActivity = false;
    }else if (from == 'activity'){
      this.isDashboard = false;
      this.isActivity = true;
    }
  }

  getDesignation(des: string) {
    return AppSettings.getDesignationValue(des);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

}
