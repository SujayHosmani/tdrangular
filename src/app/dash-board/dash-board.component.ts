import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { AppSettings, AppConstants } from '../Helpers/AppConstants';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { RegisterModel, TaskModelV01, FilterModelV01, CountModel, PaginationModel } from '../Model/register-model';
import { NetworkService } from '../Helpers/NetworService';
import { PageEvent } from '@angular/material/paginator';


function focusMeChat34(id) {
  var rowpos = $('#FocusME' + id).position();
  console.log("row ", rowpos);
  rowpos.top = rowpos.top - 154;
  $('#container').animate({ scrollTop: rowpos.top }, 600);
};

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})

export class DashBoardComponent implements OnInit, AfterViewInit {
  @Input() inputArray: RegisterModel[];
  @Input() inputProjName: string;


  isDisableLeft: boolean = false;
  isDisableRight: boolean = false;


  isProgressLeft: boolean = false;
  isProgressRight: boolean = false;

  isResolvedLeft: boolean = false;
  isResolvedRight: boolean = false;


  isClosedLeft: boolean = false;
  isClosedRight: boolean = false;

  openPageSlide: string;
  progressPageSlide: string;
  resolvedPageSlide: string;
  closedPageSlide: string;

  openPageNo: number = 1;
  progressPageNo: number = 1;
  resolvedPageNo: number = 1;
  closedPageNo: number = 1;

  paginationModel: PaginationModel;

  OpenTotalPage: number = 0;
  ProgressTotalPage: number = 0;
  ResolvedTotalPage: number = 0;
  ClosedTotalPage: number = 0;

  OpenTotalCount: number = 0;
  ProgressTotalCount: number = 0;
  ResolvedTotalCount: number = 0;
  ClosedTotalCount: number = 0;

  openTaskModelV01: TaskModelV01[] = [];
  progressTaskModelV01: TaskModelV01[] = [];
  resolvedTaskModelV01: TaskModelV01[] = [];
  closedTaskModelV01: TaskModelV01[] = [];

  isDashboard: boolean = true;
  isActivity: boolean = false;
  isTester: boolean = false;
  isDeveloper: boolean = false;
  countV01: CountModel;
  currentPageSlide: string;

  
 
  projectName: string = "";
  totalPage: number;

  

  taskModelV01: TaskModelV01[] = [];
  
  desid: any = AppConstants.designations;
  filterClass = new FilterModelV01();
  UserName: string = localStorage.getItem(AppConstants.local.usrName);
  UserId: string = localStorage.getItem(AppConstants.local.usrId);
  UserDes: string = localStorage.getItem(AppConstants.local.useDes);
  intentExtras: any;
  allRegUsers: RegisterModel[];

  SortTypeArray: any = AppConstants.SortTypeArray;
  SortStatusArray: any = AppConstants.SortStatusArray;
  SortPlatformArray: any = AppConstants.SortPlatformArray;
  SortPriorityArray: any = AppConstants.SortPriorityArray;

  constructor(public router: Router, private _location: Location, private networkService: NetworkService) {
    this.onCreate();
  }


  ngAfterViewInit(): void {
    setTimeout(() => {
      //  focusMeChat34(47);
    }, 1000);
  }

  ngOnInit(): void {
    this.allRegUsers = this.inputArray;
    this.projectName = this.inputProjName;
    if (!this.allRegUsers) {
      this.intentExtras = localStorage.getItem('projname');
      this.getUsers();
    }
  }

  onMoreClicked(taskV01: TaskModelV01) {
    // focusMeChat34(40);
    // focusMeChat34(20);
      // localStorage.setItem(AppConstants.local.chatItem,JSON.stringify(taskV01));
      localStorage.setItem(AppConstants.local.chatItem,taskV01.TaskId);
     this.router.navigate([AppConstants.navigate.chat], { state: { taskModelV01: taskV01} });
  }

  async getUsers() {
    let Users = await this.networkService.performRequest(AppConstants.method.get, AppConstants.getAllUsers, null);
    if (Users[AppConstants.res.status] == 1) {
      let reg = new RegisterModel();
      reg.UserId = 'All';
      reg.UserName = 'All';
      this.allRegUsers = Users[AppConstants.res.data];
      this.allRegUsers.unshift(reg);
    }
  }


  getWidth(from: string) {
    switch (from) {
      case 'open': {
        return (Number(this.countV01.Open) / Number(this.countV01.Total) * 100);
      }
      case 'progress': {
        return (Number(this.countV01.Progress) / Number(this.countV01.Total) * 100);
      }
      case 'resolved': {
        return (Number(this.countV01.Resolved) / Number(this.countV01.Total) * 100);
      }
      case 'closed': {
        return (Number(this.countV01.Closed) / Number(this.countV01.Total) * 100);
      }
    }

  }

  getPageSlide(fromPN: number, fromTotal: number){
    let max = (fromPN * this.filterClass.pageSize)
        let ini = (max - this.filterClass.pageSize)
        if (ini == 0) {
          if (fromTotal == 0){
            ini = 0;
          }else{
            ini = 1;
          }
          
        }
        if (max > fromTotal){
          max = fromTotal;
        }
        let val = ini + "-" + max;

        return val;
  }

  async onCreate() {
    this.filterClass = new FilterModelV01();
    // this.getTasks("create");
    this.getCount();
    this.getPaginationTasks("all", "create", 1);
  }


  // async getTasks(from: string) {
  //   this.removeAllFields();
  //   let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.filterIssuesV01, this.filterClass);
  //   if (response[AppConstants.res.status] == 1) {
  //     console.log(response)
  //     if (response[AppConstants.res.data] != null) {
        
  //       this.taskModelV01 = response[AppConstants.res.data];
  //       let max = (this.filterClass.pageNumber * this.filterClass.pageSize)
  //       let ini = (max - this.filterClass.pageSize)
  //       if (ini == 0) {
  //         ini = 1;
  //       }
  //       this.currentPageSlide = ini + "-" + max;

  //       if (this.filterClass.pageNumber == 1) {
  //         this.isDisableLeft = true;
  //       } else {
  //         this.isDisableLeft = false;
  //       }

  //       if (this.totalPage != undefined){
  //         console.log("filterClass.pageNumber = ", this.filterClass.pageNumber, ">= total pages = ", (this.totalPage) );
  //         if (Number(this.filterClass.pageNumber) >= Number(parseInt(this.totalPage.toString()))) {
  //           this.isDisableRight = true;
  //         } else {
  //           this.isDisableRight = false;
  //         }
  //       }
        

        


  //     } else {
  //       this.taskModelV01 = [];
  //       if (from == "prev") {
  //         this.filterClass.pageNumber = this.filterClass.pageNumber + 1;
  //       } else if (from = "next") {
  //         this.filterClass.pageNumber = this.filterClass.pageNumber - 1;
  //       }
  //     }
  //     this.setToAll();
  //   } else {
  //     if (from == "prev") {
  //       this.filterClass.pageNumber = this.filterClass.pageNumber + 1;
  //     } else if (from = "next") {
  //       this.filterClass.pageNumber = this.filterClass.pageNumber - 1;
  //     }
  //     this.setToAll();

  //   }
  // }


  async getPaginationTasks(from: string, where: string, pageNo: number) {
    this.paginationModel = new PaginationModel(); 
    this.removeAllFields();
    this.filterClass.from = from;
    this.filterClass.pageNumber = pageNo;
    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.filterPaginationIssuesV01, this.filterClass);
    if (response[AppConstants.res.status] == 1) {
      console.log("From Pagination =", response[AppConstants.res.data])
      if (response[AppConstants.res.data] != null){
        this.paginationModel = response[AppConstants.res.data];
        this.calculateTotalPage();
        if (from == "open" || from == "all"){
          this.OpenTotalCount = this.paginationModel.OpenCount;
          this.openTaskModelV01 = this.paginationModel.Open;
          this.openPageSlide = this.getPageSlide(this.openPageNo,  this.paginationModel.OpenCount);
          this.isDisableLeft = this.disableLeft(this.openPageNo);
          this.isDisableRight = this.disableRight(this.openPageNo, this.OpenTotalPage);

        }
        if (from == "progress" || from == "all"){
          this.ProgressTotalCount = this.paginationModel.ProgressCount;
          this.progressTaskModelV01 = this.paginationModel.Progress;
          this.progressPageSlide = this.getPageSlide(this.progressPageNo, this.paginationModel.ProgressCount);
          this.isProgressLeft = this.disableLeft(this.progressPageNo);
          this.isProgressRight = this.disableRight(this.progressPageNo, this.ProgressTotalPage);

        }
        if (from == "resolved" || from == "all"){
          this.ResolvedTotalCount = this.paginationModel.ResolvedCount;
          this.resolvedTaskModelV01 = this.paginationModel.Resolved;
          this.resolvedPageSlide = this.getPageSlide(this.resolvedPageNo, this.paginationModel.ResolvedCount);
          this.isResolvedLeft = this.disableLeft(this.resolvedPageNo);
          this.isResolvedRight = this.disableRight(this.resolvedPageNo, this.ResolvedTotalPage);

        }
        if (from == "closed" || from == "all"){
          this.ClosedTotalCount = this.paginationModel.ClosedCount;
          this.closedTaskModelV01 = this.paginationModel.Closed;
          this.closedPageSlide = this.getPageSlide(this.closedPageNo, this.paginationModel.ClosedCount);
          this.isClosedLeft = this.disableLeft(this.closedPageNo);
          this.isClosedRight = this.disableRight(this.closedPageNo, this.ResolvedTotalPage);

        }
        
      }else{
        this.setPageNoIfFails(from, where);
      }      
    }else{
      this.setPageNoIfFails(from, where);
    }
    this.setToAll();
  }

  setPageNoIfFails(from: string, where: string){
    switch(from){
      case "open": {
        if (where == "next"){
          this.openPageNo = this.openPageNo - 1;
        }else if (where == "prev"){
          this.openPageNo = this.openPageNo + 1;
        }
        break;
      }
      case "progress": {
        if (where == "next"){
          this.progressPageNo = this.progressPageNo - 1;
        }else if (where == "prev"){
          this.progressPageNo = this.progressPageNo + 1;
        }
        break;
      }
      case "resolved": {
        if (where == "next"){
          this.resolvedPageNo = this.resolvedPageNo - 1;
        }else if (where == "prev"){
          this.resolvedPageNo = this.resolvedPageNo + 1;
        }
        break;
      }
      case "closed": {
        if (where == "next"){
          this.closedPageNo = this.closedPageNo - 1;
        }else if (where == "prev"){
          this.closedPageNo = this.closedPageNo + 1;
        }
        break;
      }
    }
  }

  
  calculateTotalPage(){
    // this.totalPage = ((this.countV01.Total + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
    this.OpenTotalPage = ((this.paginationModel.OpenCount + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
    this.ProgressTotalPage = ((this.paginationModel.ProgressCount + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
    this.ResolvedTotalPage = ((this.paginationModel.ResolvedCount + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
    this.ClosedTotalPage = ((this.paginationModel.ClosedCount + (this.filterClass.pageSize - 1)) / this.filterClass.pageSize);
  }

  disableRight(fromPN: number, fromTP: number){
    if (fromTP != undefined){

      if (Number(fromPN) >= Number(parseInt(fromTP.toString()))) {
        return true;
      } else {
        return false;
      }
    }
  }

  disableLeft(fromPN: number){

    if (fromPN == 1) {
      return true;
    } else {
      return false;
    }

  }


  onSelectChoosed(value: string) {
    this.openPageNo = 1;
    this.progressPageNo = 1;
    this.resolvedPageNo = 1;
    this.closedPageNo = 1;
    this.getPaginationTasks("all","select", 1);
  }


  async getCount() {
    this.countV01 = new CountModel();
    let response = await this.networkService.performRequest(AppConstants.method.get, AppConstants.getCountV01, null);
    if (response[AppConstants.res.status] == 1) {
      this.countV01 = response[AppConstants.res.data];
    }
  }



  onOpenNextClicked(from: string) {
    if (from == "open"){
      this.openPageNo = this.openPageNo + 1;
      console.log("qweqweqwev11 ",  this.openPageNo, this.OpenTotalPage);
      this.getPaginationTasks('open', "next", this.openPageNo);
      // if (Number(this.openPageNo) <= Number(this.OpenTotalPage)) {
      //   console.log("qweqweqwev22 ",  this.openPageNo, this.OpenTotalPage);
        
      // } else {
      //   this.openPageNo = this.openPageNo - 1;
      // }
    }else if (from == "progress"){
      this.progressPageNo = this.progressPageNo + 1;
      console.log("qweqweqwev33 ",  this.progressPageNo, this.ProgressTotalPage);
      this.getPaginationTasks(from, "next", this.progressPageNo);
     
    }else if (from == "resolved"){
      this.resolvedPageNo = this.resolvedPageNo + 1;
      this.getPaginationTasks(from, "next", this.resolvedPageNo);
    
    }else if (from == "closed"){
      this.closedPageNo = this.closedPageNo + 1;
      this.getPaginationTasks(from, "next", this.closedPageNo);
    }
    

  }

  onOpenPreviousClicked(from: string) {
    if (from == "open"){
      if (this.openPageNo != 1) {
        this.openPageNo = this.openPageNo - 1;
        this.getPaginationTasks("open","prev", this.openPageNo);
      }
    }else if (from == "progress"){
      if (this.progressPageNo != 1) {
        this.progressPageNo = this.progressPageNo - 1;
        this.getPaginationTasks("progress","prev", this.progressPageNo);
      }
    }else if (from == "resolved"){
      if (this.resolvedPageNo != 1) {
        this.resolvedPageNo = this.resolvedPageNo - 1;
        this.getPaginationTasks("resolved","prev", this.resolvedPageNo);
      }
    }else if (from == "closed"){
      if (this.closedPageNo != 1) {
        this.closedPageNo = this.closedPageNo - 1;
        this.getPaginationTasks("closed","prev", this.closedPageNo);
      }
    }
    

  }

  removeAllFields() {
    if (this.filterClass.Status == 'All') {
      this.filterClass.Status = undefined;
    }
    if (this.filterClass.TaskType == 'All') {
      this.filterClass.TaskType = undefined;
    }
    if (this.filterClass.TaskById == 'All') {
      this.filterClass.TaskById = undefined;
    }
    if (this.filterClass.TaskToId == 'All') {
      this.filterClass.TaskToId = undefined;
    }
    if (this.filterClass.Platform == 'All') {
      this.filterClass.Platform = undefined;
    }
    if (this.filterClass.Priority == 'All') {
      this.filterClass.Priority = undefined;
    }

  }

  setToAll() {
    if (this.filterClass.Status == undefined) {
      this.filterClass.Status = 'All';
    }
    if (this.filterClass.TaskType == undefined) {
      this.filterClass.TaskType = 'All';
    }
    if (this.filterClass.TaskById == undefined) {
      this.filterClass.TaskById = 'All';
    }
    if (this.filterClass.TaskToId == undefined) {
      this.filterClass.TaskToId = 'All';
    }
    if (this.filterClass.Platform == undefined) {
      this.filterClass.Platform = 'All';
    }
    if (this.filterClass.Priority == undefined) {
      this.filterClass.Priority = 'All';
    }
  }

  isTeam(taskV01: TaskModelV01) {
    if (taskV01.TaskToName.includes('Team')) {
      return true;
    }
  }

  isReportedAndClosedSame(taskV01: TaskModelV01) {
    if (taskV01.TaskById == taskV01.DevId) {
      return true;
    }
  }


  showPage(from: string) {
    if (from == 'dashboard') {
      this.isDashboard = true;
      this.isActivity = false;
      this.isTester = false;
      this.isDeveloper = false;
    } else if (from == 'activity') {
      this.isDashboard = false;
      this.isActivity = true;
      this.isTester = false;
      this.isDeveloper = false;
    } else if (from == 'tester') {
      this.isDashboard = false;
      this.isActivity = false;
      this.isTester = true;
      this.isDeveloper = false;
    } else if (from == 'developer') {
      this.isDashboard = false;
      this.isActivity = false;
      this.isTester = false;
      this.isDeveloper = true;
    }
  }

  getDesignation(des: string) {
    return AppSettings.getDesignationValue(des);
  }

}
