import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NetworkService } from '../Helpers/NetworService';
import { AppConstants } from '../Helpers/AppConstants'
import { RegisterModel } from '../Model/register-model';


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  userPresent: string = localStorage.getItem(AppConstants.local.usrName);
  userName: string = " " + localStorage.getItem(AppConstants.local.usrName);
  allUsers: RegisterModel[];

  constructor(
    public router: Router,
    public networkService: NetworkService
  ) {
    try{
      let val = this.router.getCurrentNavigation().extras.state.from;
      if(val != null){
        window.location.reload();
      }
    }catch(e){

    }
   
   }

  async ngOnInit() {

      let allUsers = await this.networkService.performRequest(AppConstants.method.get, AppConstants.getAllUsers, null);
      if (allUsers[AppConstants.res.status] == 1) {
        let reg = new RegisterModel();
        reg.UserId = 'All';
        reg.UserName = 'All';
        this.allUsers = allUsers[AppConstants.res.data];
        this.allUsers.unshift(reg);
        // this.reloadComponent();

    }



  }

  login() {
    this.router.navigate([AppConstants.navigate.login]);
  }

  logout() {
    if (confirm("Are you sure you want to Logout" + name)) {
      this.networkService.logOut();
      this.reloadComponent()
    }

  }

  reloadComponent() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([AppConstants.navigate.project]);
  }

  async goto(place: string) {
    let isLogin = localStorage.getItem(AppConstants.local.isLogin);
    let userId = localStorage.getItem(AppConstants.local.usrId);
    console.log("in projects: " + userId);
    if (isLogin) {
      localStorage.setItem('projname',place);
      
      this.router.navigate([AppConstants.navigate.main]);
      // this.reloadComponent();
      // this.router.navigate([AppConstants.navigate.main], { state: { from: place, users: this.allUsers } });
    } else {
      this.router.navigate([AppConstants.navigate.login]);
    }

  }

}
