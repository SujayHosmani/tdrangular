export class Message{
    public taskId : string;
    public userName : string;
    public msg : string;
    public date : any;
    public userId: string;
    public id: any;
}