export class RegisterModel {
    public UserName: string;
    public UserId: string;
    public Password: string;
    public UserType: string;
    public UserDesignation: string;
}

export class LoginModel {
    public UserId: string;
    public Password: string;
}

export class IssueMainModelConvertor {
    public TaskTitle: string;
    public TaskDescription: string;
    public Platform: string;
    public TaskBy: string;
    public TaskTo: string;
    public From: string;
    public To: string;
    public Priority: string;
    public Date: any;

    public Accepted: string;
    public Rejected: string;
    public Solved: string;
    public UnSolved: string;
    public DevName: string;
    public DevId: string;
    public ProjectName: string;
    public DevRemark: string;

    public Resolved: string;
    public UnResolved: string;
    public TesterRemark: string;

    public Started: string;

    public ReIssueId: any;
    public ReIssued: string;

    public Img: Array<string>;
    public TaskId: any;
    public Images: string;
}

export class IssueMainModel {
    public TaskTitle: string;
    public TaskDescription: string;
    public Platform: string;
    public TaskBy: string;
    public TaskTo: string;
    public From: string;
    public To: string;
    public Priority: string;
    public Date: any;

    public Accepted: boolean;
    public Rejected: boolean;
    public Solved: boolean;
    public UnSolved: boolean;
    public DevName: string;
    public DevId: string;
    public ProjectName: string;
    public DevRemark: string;

    public Resolved: boolean;
    public UnResolved: boolean;
    public TesterRemark: string;

    public Started: boolean;

    public ReIssueId: any;
    public ReIssued: boolean;

    public Img: Array<string | number>;
    public TaskId: any;
    public Images: string;
}

export class TaskDescFilter {
    public TaskBy: string;
    public TaskTo: string;
    public Platform: string;
    public Accepted: string;
    public Rejected: string;
    public Solved: string;
    public UnSolved: string;
    public ReSolved: string;
    public UnReSolved: string;
    public Date: string;
}

export class SendImgModel {
    public Id: any;
    public Img: string;
    public TaskId: string;
    public Type: string;
}


export class ImgModel {
    public ImageFileName: string;
    public ImageBase64: string;
    public ImageType: string;
    public isUploading: boolean;
    public receivedDocid: string;
    public isUploaded: boolean;
    public isUploadFailed: boolean;

}

export class CountModel {
    public Open: number;
    public Progress: number;
    public Resolved: number;
    public Closed: number;
    public Reopened: number;
    public Total: number;
}

export class TaskModelV01 {
    public TaskId: any;
    public TaskType: string;
    public Status: string;
    public DevStatus: string;
    public TestStatus: string;
    public DevRemark: string;
    public TestRemark: string;
    public ImgCount: number;
    public ImgArray: Array<string | number>;
    public TaskSub: string;
    public TaskDesc: string;
    public DevName: string;
    public DevId: string;
    public TaskById: string;
    public TaskToId: string;
    public TaskByName: string;
    public TaskToName: string;
    public Platform: string;
    public Priority: string;
    public ProjName: string;

    public CreatedDt: any;
    public ModifiedDt: any;

    public Started: string;
    public ReOpened: string;
    public ReOpenedId: string;

    public From: string;
    public Flow: string;

    public MovedBy: string;
    public MovedById: string;
}

export class FilterModelV01 {
    public TaskType: string;
    public Status: string;
    public TaskById: string;
    public TaskToId: string;
    public Platform: string;
    public Priority: string;
    public CreatedDt: string;
    public ModifiedDt: string;
    public DevStatus: string;
    public TestStatus: string;
    public DevId: string;
    public Started: string;
    public Reopened: string;
    public ProjName: string;
    public Team: string;
    public from: String;
    public pageNumber: number = 1;
    public pageSize: number = 6;
}

export class PaginationModel {
    public Open: TaskModelV01[];
    public Progress: TaskModelV01[];
    public Resolved: TaskModelV01[];
    public Closed: TaskModelV01[];
    public Reopened: TaskModelV01[];
    public OpenCount: number;
    public ProgressCount: number;
    public ResolvedCount: number;
    public ClosedCount: number;
    public ReopenedCount: number;
    public TotalCount: number;
    
}

export class WorkFlow {
    public id: any;
    public taskId: any;
    public taskType: string;
    public taskSub: string;
    public status: string;
    public devStatus: string;
    public testStatus: string;
    public taskDesc: string;
    public platform: string;
    public testRemark: string;
    public devRemark: string;
    public taskById: string;
    public taskToId: string;
    public taskByName: string;
    public taskToName: string;
    public priority: string;
    public projName: string;
    public createdDt: any;
    public from: string;
    public to: string;
    public flow: string;
    public role: string;
    public devName: string;
    public devId: string;
    public movedby: string;
    public movedbyId: string;
}

export class WorkFlowC {
    public Id: any;
    public TaskId: any;
    public TaskType: string;
    public TaskSub: string;
    public Status: string;
    public DevStatus: string;
    public TestStatus: string;
    public TaskDesc: string;
    public Platform: string;
    public TestRemark: string;
    public DevRemark: string;
    public TaskById: string;
    public TaskToId: string;
    public TaskByName: string;
    public TaskToName: string;
    public Priority: string;
    public ProjName: string;
    public CreatedDt: any;
    public From: string;
    public To: string;
    public Flow: string;
    public Role: string;
    public DevName: string;
    public DevId: string;
    public Movedby: string;
    public MovedbyId: string;
}