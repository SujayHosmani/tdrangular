import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AppConstants, AppSettings } from '../Helpers/AppConstants'
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent, MatChip } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { IssueMainModel, RegisterModel, TaskDescFilter, ImgModel, SendImgModel, IssueMainModelConvertor } from '../Model/register-model';
import { NetworkService } from '../Helpers/NetworService';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogOverviewExampleDialog } from '../Dialog/dialog-overview-example-dialog';
import { async } from '@angular/core/testing';

function focusMe() {
  var id = localStorage.getItem('taskid');
  var rowpos = $('#FocusME' + id).position();
  if (rowpos) {
    console.log("row ", rowpos.top, localStorage.getItem('taskid'));
    rowpos.top = rowpos.top + 180;
    // $('#container').animate({ scrollTop: rowpos.top }, 600);
    document.body.scrollTop = rowpos.top;
    document.documentElement.scrollTop = rowpos.top;
  } else {
    localStorage.setItem('taskid', null);
  }

};

export interface DialogData {
  remark: string;
  name: string;
  title: string;
  description: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {
  isFilter: boolean = true;
  isShowFilter: boolean = false;
  isChatClicked: boolean = false;
  statusMessage: string = "";
  DevSelectStatus: string;
  isExtented: boolean = false;
  TestSelectStatus: string;
  SortDate: string;
  SortPriority: string;
  statusMessageTest: string = "";
  remarkTest: string = "";
  remark: string = "";
  toast: string = "";
  intentExtras: any;
  loading: number = 0;
  images: ImgModel[] = [];
  platform: string = 'all';
  isRaiseIssue: boolean = false;
  isRaiseImg: boolean = false;
  MainIssueName: string;
  filesStatus: string = "No files Attached";
  isIssueSubmitted: boolean = false;
  MainAssignName: string;
  docsId = [];
  platform1: string;
  issueModel: IssueMainModel;
  taskModel: IssueMainModel[] = [];
  taskModelConvertor: IssueMainModelConvertor[] = [];
  title: string = "d";
  isAssigned: boolean = false;
  isMinimized: boolean = false;
  isImgMinimized: boolean = false
  UserName: string = localStorage.getItem(AppConstants.local.usrName);
  UserId: string = localStorage.getItem(AppConstants.local.usrId);
  UserDes: string = localStorage.getItem(AppConstants.local.useDes);
  designations: any;
  filterClass = new TaskDescFilter();
  visible = true;
  DevSortArray = AppConstants.sortDevArray;
  TestSortArray = AppConstants.sortTestArray;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  FilterDateNew:any;
  allRegUsers: RegisterModel[];

  IssueCtrl = new FormControl();
  filteredRegUsersIssue: Observable<RegisterModel[]>;
  userChipArray: RegisterModel[] = [];

  MainIssueCtrl = new FormControl();
  filteredMainRegUsersIssue: Observable<RegisterModel[]>;

  MainAssignCtrl = new FormControl();
  filteredMainRegUsersAssign: Observable<RegisterModel[]>;

  @ViewChild('IssueInput') IssueInput: ElementRef<HTMLInputElement>;
  @ViewChild('MainIssueInput') MainIssueInput: ElementRef<HTMLInputElement>;
  @ViewChild('MainAssignInput') MainAssignInput: ElementRef<HTMLInputElement>;
  @ViewChild('autoIssue') matAutocompleteIssue: MatAutocomplete;
  @ViewChild('autoMainIssue') matAutocompleteMainIssue: MatAutocomplete;
  @ViewChild('autoMainAssign') matAutocompleteMainAssign: MatAutocomplete;
  @ViewChild('ImgInput') ImgInputVariable: ElementRef;


  constructor(route: ActivatedRoute, public dialog: MatDialog, public router: Router, private _location: Location, private networkService: NetworkService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      var id = localStorage.getItem('taskid')
      console.log("Home Constructor 2 ", id);
      if (!this.isChatClicked) {
        setTimeout(() => {
          focusMe();
        }, 100);
      }


      return false;
    };
    this.onCreate();
    try {
      this.intentExtras = this.router.getCurrentNavigation().extras.state.from;
      this.allRegUsers = this.router.getCurrentNavigation().extras.state.users;
      if (this.intentExtras) {
        this.onCreate();
      } else {
          // this._location.back();
      }
    } catch (error) {
      console.log("errors ", error);
      //  this._location.back();
    }
  }
  ngAfterViewInit(): void {
    console.log("Home NGAFTER INIT");
    setTimeout(() => {
      if (this.UserDes == 'web' || this.UserDes == 'api' || this.UserDes == 'android' || this.UserDes == 'ios') {
        // this.MainAssignInput.nativeElement.value = this.UserName;
      } else if (this.UserDes == 'tester') {
        this.MainIssueInput.nativeElement.value = this.UserName;
      }
    }, 1000);

  }


  async onCreate() {
    console.log("On create");
    this.removeFirstElement()
    this.filteredRegUsersIssue = this.IssueCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: RegisterModel | null) => fruit ? this._filter(fruit) : this.allRegUsers.slice()));

    this.filteredMainRegUsersIssue = this.MainIssueCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: RegisterModel | null) => fruit ? this._filter(fruit) : this.allRegUsers.slice()));

    this.filteredMainRegUsersAssign = this.MainAssignCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: RegisterModel | null) => fruit ? this._filter(fruit) : this.allRegUsers.slice()));

    this.title = "Welcome to TDR " + "   -   " + this.intentExtras;
    this.designations = AppConstants.designations;
    this.issueModel = new IssueMainModel();
    this.filterClass = new TaskDescFilter();

    if (this.UserDes == 'web' || this.UserDes == 'api' || this.UserDes == 'android' || this.UserDes == 'ios') {
      this.platform = this.UserDes;
      this.filterClass.Platform = this.UserDes;
    } else if (this.UserDes == 'tester') {
      this.filterClass.TaskBy = this.UserId;
      this.filterClass.Platform = undefined;
      this.platform = 'all';
    } else {
      this.filterClass.Platform = undefined;
      this.platform = 'all';
    }

    this.callFilteredIssues();

  }


  reloadComponent() {
    console.log("reload component")
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([AppConstants.navigate.home]);
  }

  isCurrentUser(user: RegisterModel) {
    if (this.UserId == user.UserId || user.UserId == 'All') {
      return true;
    } else {
      return false;
    }
  }

  getDesignation(des: string) {
    return AppSettings.getDesignationValue(des);
  }

  onExtendedClicked() {
    if (this.isExtented) {
      this.isExtented = false;
    } else {
      this.isExtented = true;
    }
  }

  removeFirstElement() {
    const index = this.userChipArray.indexOf(this.userChipArray[0]);
    if (index >= 0) {
      this.userChipArray.splice(index, 1);
      this.isAssigned = false;
    }
  }

  onMinimizedClicked() {
    if (this.isMinimized) {
      this.isMinimized = false;
    } else {
      this.isMinimized = true;
    }
  }

  onImgMinimizedClicked() {
    if (this.isImgMinimized) {
      this.isImgMinimized = false;
    } else {
      this.isImgMinimized = true;
    }
  }

  onRaiseClicked() {
    this.isIssueSubmitted = false;
    this.isMinimized = false;
    if (this.isRaiseIssue) {
      this.isRaiseIssue = false;
      this.isRaiseImg = false;
    } else {
      this.isRaiseIssue = true;
    }
  }

  onRaiseImgClicked() {
    this.isImgMinimized = false;
    if (this.isRaiseImg) {
      this.isRaiseImg = false;
    } else {
      this.isRaiseImg = true;
    }
  }

  onFileChange(event) {

    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event2: any) => {
          let Img = new ImgModel();
          Img.ImageBase64 = event2.target.result
          Img.ImageFileName = event.target.files[i].name
          Img.isUploading = true;
          Img.ImageType = event.target.files[i].type;
          this.images.push(Img);
          this.submitAllImages(this.images[this.images.length - 1], filesAmount, i + 1);
        }
        reader.readAsDataURL(event.target.files[i]);
      }


    }
  }


  async submitAllImages(img: ImgModel, total: number, current: number) {
    console.log("The total ", total, " Current ", current);
    let sendImg = new SendImgModel();
    sendImg.Img = img.ImageBase64;
    sendImg.Type = this.UserId + "_" + img.ImageFileName;

    img.isUploading = true;
    img.isUploadFailed = false;
    img.isUploaded = false

    this.getFileStatus();
    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.sendImage, sendImg);
    if (response[AppConstants.res.status] == 1) {
      if (total == current) {
        if (this.ImgInputVariable) {
          if (this.ImgInputVariable.nativeElement) {
            this.ImgInputVariable.nativeElement.value = "";
          }
        }
      }
      img.isUploading = false;
      img.isUploaded = true
      img.receivedDocid = response[AppConstants.res.data];
      this.getFileStatus();
    } else {
      if (total == current) {
        if (this.ImgInputVariable) {
          if (this.ImgInputVariable.nativeElement) {
            this.ImgInputVariable.nativeElement.value = "";
          }
        }
      }
      img.isUploadFailed = true;
      img.isUploading = false;
      img.isUploaded = false
      this.getFileStatus();

    }
    this.getFileStatus();

  }

  getFileStatus() {
    var sucess = 0;
    var fail = 0;
    this.loading = 0;
    var status = "";
    for (var i = 0; i < this.images.length; i++) {
      if (this.images[i].isUploaded) {
        sucess = sucess + 1
      } else if (this.images[i].isUploadFailed) {
        fail = fail + 1;
      } else if (this.images[i].isUploading) {
        this.loading = this.loading + 1;
      }

    }

    status = ""
    if (sucess > 0) {
      status = "Files uploaded : " + sucess;
    }

    if (fail > 0) {
      status = status + ",   Failed files : " + fail;
    }

    if (this.loading > 0) {
      status = status + ",   Uploading files : " + this.loading;
    }

    if (sucess == 0 && fail == 0 && this.loading == 0) {
      this.filesStatus = "No files Attached";
    } else {
      this.filesStatus = status;
    }


  }


  onIntoClicked() {
    this.onRaiseClicked()
  }

  onIntoImgClicked() {
    this.onRaiseImgClicked();
  }



  checkForExistence(name: string): boolean {
    let val = name.toLowerCase().trim()
    return this.allRegUsers.some(r => r.UserName.toLowerCase().trim() === val);
  }

  add(event: MatChipInputEvent): void {
    console.log("add event");
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      if (this.userChipArray.length == 0) {
        if (this.checkForExistence(value)) {
          let val = this.allRegUsers.filter(book => book.UserName.toLowerCase().trim() === value.toLowerCase().trim());
          this.userChipArray.push(val[0]);
          this.isAssigned = true;
        }
      }
    }

    if (input) {
      input.value = '';
    }

    this.IssueCtrl.setValue(null);
  }

  resetDevBooleanFields() {
    this.filterClass.Accepted = undefined;
    this.filterClass.Rejected = undefined;
    this.filterClass.Solved = undefined;
    this.filterClass.UnSolved = undefined;
    this.filterClass.ReSolved = undefined;
    this.filterClass.UnReSolved = undefined;
  }

  ApplySortFilter() {
    //dev status
    //test status
    //date wise
    //date sort 
    //priority sort
    this.hideFilterView();
    if (this.DevSelectStatus) {
      switch (this.DevSelectStatus) {
        case 'Pending to respond': {
          this.filterClass.Accepted = AppConstants.No;
          this.filterClass.Rejected = AppConstants.No;

          this.filterClass.Solved = undefined;
          this.filterClass.UnSolved = undefined;
          this.filterClass.ReSolved = undefined;
          this.filterClass.UnReSolved = undefined;
          break;
        }
        case 'In progress': {
          this.filterClass.Accepted = AppConstants.yes;
          this.filterClass.Rejected = AppConstants.No;
          this.filterClass.Solved = AppConstants.No;
          this.filterClass.UnSolved = AppConstants.No;

          this.filterClass.ReSolved = undefined;
          this.filterClass.UnReSolved = undefined;
          break;
        }
        case 'Rejected': {
          this.filterClass.Rejected = AppConstants.yes;

          this.filterClass.Accepted = undefined;
          this.filterClass.Solved = undefined;
          this.filterClass.UnSolved = undefined;
          this.filterClass.ReSolved = undefined;
          this.filterClass.UnReSolved = undefined;
          break;
        }
        case 'Solved': {
          this.filterClass.Solved = AppConstants.yes;

          this.filterClass.Accepted = undefined;
          this.filterClass.Rejected = undefined;
          this.filterClass.UnSolved = undefined;
          this.filterClass.ReSolved = undefined;
          this.filterClass.UnReSolved = undefined;
          break;
        }
        case 'Un Solved': {
          this.filterClass.UnSolved = AppConstants.yes;

          this.filterClass.Accepted = undefined;
          this.filterClass.Rejected = undefined;
          this.filterClass.Solved = undefined;
          this.filterClass.ReSolved = undefined;
          this.filterClass.UnReSolved = undefined;
          break;
        }
        case 'none': {
          this.resetDevBooleanFields();
          break;
        }
        default: {
          this.resetDevBooleanFields();
          break;
        }
      }
    }

    if (this.TestSelectStatus) {
      if (this.DevSelectStatus != 'Pending to respond' && this.DevSelectStatus != 'n progress'){
        switch (this.TestSelectStatus) {
          case 'Pending Verification': {
            this.filterClass.ReSolved = AppConstants.No;
            this.filterClass.UnReSolved = AppConstants.No;
            break;
          }
          case 'Resolved': {
            this.filterClass.ReSolved = AppConstants.yes;
            this.filterClass.UnReSolved = undefined;
            break;
          }
          case 'Un Resolved': {
            this.filterClass.ReSolved = undefined;
            this.filterClass.UnReSolved = AppConstants.yes;
            break;
          }
          case 'none': {
            this.filterClass.ReSolved = undefined;
            this.filterClass.UnReSolved = undefined;
            break;
          }
          default: {
            this.resetDevBooleanFields();
            break;
          }
        }
      }
     
    }
    this.sortAndFilterData();

  }

  remove(fruit: RegisterModel): void {
    console.log("REMOVE");
    const index = this.userChipArray.indexOf(fruit);

    if (index >= 0) {
      this.userChipArray.splice(index, 1);
      this.isAssigned = false;
    }

    if (this.platform1) {
      if (this.platform1 && fruit.Password != 'new')
        this.onClickPlatform1(this.platform1);
    }

  }

  selected(event: MatAutocompleteSelectedEvent): void {
    console.log("Selected bottom", event);
    if (this.userChipArray.length == 0) {
      this.userChipArray.push(event.option.value);
      this.isAssigned = true;
      this.IssueInput.nativeElement.value = '';
      this.IssueCtrl.setValue(null);
    }
    this.IssueInput.nativeElement.value = '';
    this.IssueCtrl.setValue(null);

  }

  onSelectMainIssue(event: MatAutocompleteSelectedEvent): void {
    console.log("Selected Main Issue", event.option.value.UserName);
    this.MainIssueInput.nativeElement.value = event.option.value.UserName;
    // this.MainIssueCtrl.setValue(null);
    this.MainIssueName = event.option.value.UserName;
    this.filterClass.TaskBy = event.option.value.UserId;
    this.sortAndFilterData();

  }

  onSelectMainAssign(event: MatAutocompleteSelectedEvent): void {
    console.log("Selected Main Assign", event.option.value.UserName);
    this.MainAssignInput.nativeElement.value = event.option.value.UserName;
    // this.MainAssignCtrl.setValue(null);
    this.MainAssignName = event.option.value.UserName
    this.filterClass.TaskTo = event.option.value.UserId;
    this.sortAndFilterData()

  }

  attachImages() {
    this.isRaiseImg = true;
    this.isImgMinimized = false;
  }

  onClickPlatform(id: string) {
    this.platform = id;
    this.filterClass.Platform = id;
    if (id == "all") {
      this.filterClass.Platform = undefined;
    }
    this.sortAndFilterData();
  }

  onClickPlatform1(id: string) {
    this.platform1 = id;
    this.issueModel.Platform = id;
    if (this.userChipArray.length > 0) {
      if (this.userChipArray[0].Password == 'new') {
        this.removeFirstElement();
      }
    }
    if (this.userChipArray.length == 0) {
      var regNew = new RegisterModel();
      regNew.Password = 'new'
      regNew.UserId = id
      this.isAssigned = true;
      switch (id) {
        case 'web': {
          regNew.UserName = 'WEB Team'
          this.userChipArray.push(regNew);
          break;
        }
        case 'api': {
          regNew.UserName = 'API Team'
          this.userChipArray.push(regNew)
          break;
        }
        case 'android': {
          regNew.UserName = 'Android Team'
          this.userChipArray.push(regNew)
          break;
        }
        case 'ios': {
          regNew.UserName = 'iOS Team'
          this.userChipArray.push(regNew)
          break;
        }
      }
    }

  }

  private _filter(value: any): RegisterModel[] {
    if (value) {


      const filterValue = value.toLowerCase().trim();
      let ff = this.allRegUsers.filter(option =>
        option.UserName.toLowerCase().includes(filterValue) ||
        option.UserName.toLowerCase().includes(filterValue));
      console.log("filter ffd 1", ff.length);
      return ff
    } else {
      return [];
    }

  }



  onSucessClosedClicked() {
    this.isIssueSubmitted = false;
  }

  showDescriptionWaiting(task: IssueMainModel) {
    if (!task.Rejected && !task.Solved && !task.UnSolved) {
      this.statusMessageTest = "Waiting"
      this.remarkTest = "Waiting"
      return true;
    }
  }

  showDescriptionPending(task: IssueMainModel) {
    if (task.Rejected || task.Solved || task.UnSolved) {
      if (!task.UnResolved && !task.Resolved) {
        this.statusMessageTest = "Pending"
        this.remarkTest = "Pending"
        return true;
      } else {
        if (task.Resolved) {
          this.statusMessageTest = "Resolved"
          this.remarkTest = task.TesterRemark ? task.TesterRemark : "Resolved"
          return false;
        } else if (task.UnResolved) {
          this.statusMessageTest = "Un Resolved"
          this.remarkTest = task.TesterRemark ? task.TesterRemark : "Un Resolved"
          return false;
        }
      }
    }

  }

  showDescriptionResolved(task: IssueMainModel) {
    if (task.Rejected || task.Solved || task.UnSolved) {
      if (task.UnResolved || task.Resolved) {
        if (task.Resolved) {
          this.statusMessageTest = "Resolved"
          this.remarkTest = task.TesterRemark ? task.TesterRemark : "Resolved"
          return true;
        } else if (task.UnResolved) {
          this.statusMessageTest = "Un Resolved"
          this.remarkTest = task.TesterRemark ? task.TesterRemark : "Un Resolved"
          return true;
        }
      }
    }
    return false;

  }

  isTestedByMeAndPreFinal(task: IssueMainModel) {
    if ((task.Rejected || task.Solved || task.UnSolved)) {
      if (!task.Resolved && !task.UnResolved) {
        if (task.TaskBy == this.UserId) {
          return true;
        }
      }
    }
    return false;
  }

  isPreFinal(task: IssueMainModel) {
    if ((task.Accepted) && (!task.Rejected && !task.Solved && !task.UnSolved)) {
      if (task.DevId == this.UserId) {
        return true
      }
    }
    return false;
  }

  toggleFilterSort(from: string) {
    if (from == "filter") {
      this.isFilter = true;
    } else {
      this.isFilter = false;

    }
  }

  async resetAll(from: string) {
    this.filterClass = new TaskDescFilter();
    this.MainIssueName = '';
    this.MainAssignName = '';
    this.SortDate = null;
    this.SortPriority = null;
    this.platform = this.UserDes;
    this.TestSelectStatus = undefined;
    this.DevSelectStatus = undefined;
    this.ngAfterViewInit();
    this.hideFilterView();
    if (this.UserDes == 'web' || this.UserDes == 'api' || this.UserDes == 'android' || this.UserDes == 'ios') {
      this.platform = this.UserDes;
      this.filterClass.Platform = this.UserDes;
    } else if (this.UserDes == 'tester') {
      this.filterClass.Platform = undefined;
      this.platform = 'all';
      this.filterClass.TaskBy = this.UserId;
      this.MainIssueInput.nativeElement.value = this.UserName;
    } else {
      this.filterClass.Platform = undefined;
      this.platform = 'all';
    }

    if (from != 'onSubmit') {
      this.callFilteredIssues();
    }



  }


  async callFilteredIssues() {
    let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.filterIssues, this.filterClass);
    if (response[AppConstants.res.status] == 1) {
      this.taskModel = [];
      this.taskModelConvertor = response[AppConstants.res.data];
      await this.convertorToTaskModel(this.taskModelConvertor);
    }
  }

  hideFilterView() {
    this.isShowFilter = false;
  }

  showFilter() {
    this.isShowFilter = true;
  }

  getStatus(task: IssueMainModel) {
    // solved or unsolved 
    // accepted or rejected
    if (task.Rejected) {
      this.statusMessage = "Rejected"
      this.remark = task.DevRemark ? task.DevRemark : "No remarks";
      return true;
    } else if (task.Accepted) {
      if (task.Solved) {
        this.statusMessage = "Solved"
        this.remark = task.DevRemark ? task.DevRemark : "No remarks";
        return true;
      } else if (task.UnSolved) {
        this.statusMessage = "Un Solved"
        this.remark = task.DevRemark ? task.DevRemark : "No remarks";
        return true;
      } else {
        this.statusMessage = "Accepted"
        this.remark = task.DevRemark ? task.DevRemark : "in progress";
        return true;
      }

    } else {
      this.statusMessage = "Yet to respond"
      this.remark = task.DevRemark ? task.DevRemark : "No remarks";
      return true;
    }

  }

  getFilterCount(){
    // return "(4)";
    var cnt = 0;
    if (this.filterClass){
      if (this.filterClass.Platform){
        cnt += 1;
      }
      if (this.filterClass.TaskBy){
        cnt += 1;
      }
      if (this.filterClass.TaskTo){
        cnt += 1;
      }
      if (this.filterClass.Date){
        cnt += 1;
      }
      if (this.filterClass.Accepted || this.filterClass.Rejected || this.filterClass.Solved || this.filterClass.UnReSolved){
        cnt += 1;
      }
      if (this.filterClass.ReSolved || this.filterClass.UnReSolved){
        cnt += 1;
      }
      if (cnt > 0){
        return "(" + cnt + ")"
      }
      
    }
  }

  onChatClicked(task: IssueMainModel) {
    this.isChatClicked = true;
    localStorage.setItem('taskid', task.TaskId)
    this.router.navigate([AppConstants.navigate.chat], { state: { taskModel: this.taskModel, task: task } });
  }

  isTeamOrMe(task: IssueMainModel) {
    if (!task.Accepted && !task.Rejected) {
      if (task.TaskTo == this.UserId || task.TaskTo == this.UserDes) {
        return true;
      } else {
        return false;
      }
    }
  }

 async convertorToTaskModel(taskModelConvert: IssueMainModelConvertor[]) {
   if (taskModelConvert){
    for (var i = 0; i < taskModelConvert.length; i++) {
      let taskModel = new IssueMainModel();

      taskModel.Img = taskModelConvert[i].Img;

      taskModel.Platform = taskModelConvert[i].Platform;

      taskModel.Priority = taskModelConvert[i].Priority;

      taskModel.ProjectName = taskModelConvert[i].ProjectName;

      taskModel.ReIssueId = taskModelConvert[i].ReIssueId;

      taskModel.TaskBy = taskModelConvert[i].TaskBy;

      taskModel.TaskDescription = taskModelConvert[i].TaskDescription;

      taskModel.TaskId = taskModelConvert[i].TaskId;

      taskModel.TaskTitle = taskModelConvert[i].TaskTitle;

      taskModel.TaskTo = taskModelConvert[i].TaskTo;

      taskModel.TesterRemark = taskModelConvert[i].TesterRemark;

      taskModel.To = taskModelConvert[i].To;

      taskModel.Date = taskModelConvert[i].Date;

      taskModel.DevId = taskModelConvert[i].DevId;

      taskModel.DevName = taskModelConvert[i].DevName;

      taskModel.DevRemark = taskModelConvert[i].DevRemark;

      taskModel.From = taskModelConvert[i].From;

      taskModel.Images = taskModelConvert[i].Images;

      if (taskModelConvert[i].ReIssued) {
        if (taskModelConvert[i].ReIssued == AppConstants.yes) {
          taskModel.ReIssued = true;
        }else{
          taskModel.ReIssued = false;
        }
      }

      if (taskModelConvert[i].Rejected) {
        if (taskModelConvert[i].Rejected == AppConstants.yes) {
          taskModel.Rejected = true;
        }else{
          taskModel.Rejected = false;
        }
      }

      if (taskModelConvert[i].Resolved) {
        if (taskModelConvert[i].Resolved == AppConstants.yes) {
          taskModel.Resolved = true;
        }else{
          taskModel.Resolved = false;
        }
      }

      if (taskModelConvert[i].Solved) {
        if (taskModelConvert[i].Solved == AppConstants.yes) {
          taskModel.Solved = true;
        }else{
          taskModel.Solved = false;
        }
      }




      if (taskModelConvert[i].Started) {
        if (taskModelConvert[i].Started == AppConstants.yes) {
          taskModel.Started = true;
        }else{
          taskModel.Started = false;
        }
      }

      if (taskModelConvert[i].UnResolved) {
        if (taskModelConvert[i].UnResolved == AppConstants.yes) {
          taskModel.UnResolved = true;
        }else{
          taskModel.UnResolved = false;
        }
      }

      if (taskModelConvert[i].UnSolved) {
        if (taskModelConvert[i].UnSolved == AppConstants.yes) {
          taskModel.UnSolved = true;
        }else{
          taskModel.UnSolved = false;
        }
      }

      if (taskModelConvert[i].Accepted) {
        if (taskModelConvert[i].Accepted == AppConstants.yes) {
          taskModel.Accepted = true;
        }else{
          taskModel.Accepted = false;
        }
      }

      this.taskModel.push(taskModel);

    }
   }
    
  }




  async TaskModelToConvertor(taskModel: IssueMainModel) {
      let taskModelConvertor = new IssueMainModelConvertor();

      taskModelConvertor.Platform = taskModel.Platform;

      taskModelConvertor.Priority = taskModel.Priority;

      taskModelConvertor.ProjectName = taskModel.ProjectName;

      taskModelConvertor.ReIssueId = taskModel.ReIssueId;

      taskModelConvertor.TaskBy = taskModel.TaskBy;

      taskModelConvertor.TaskDescription = taskModel.TaskDescription;

      taskModelConvertor.TaskId = taskModel.TaskId;

      taskModelConvertor.TaskTitle = taskModel.TaskTitle;

      taskModelConvertor.TaskTo = taskModel.TaskTo;

      taskModelConvertor.TesterRemark = taskModel.TesterRemark;

      taskModelConvertor.To = taskModel.To;

      taskModelConvertor.Date = taskModel.Date;

      taskModelConvertor.DevId = taskModel.DevId;

      taskModelConvertor.DevName = taskModel.DevName;

      taskModelConvertor.DevRemark = taskModel.DevRemark;

      taskModelConvertor.From = taskModel.From;

      taskModelConvertor.Images = taskModel.Images;

      if (taskModel.ReIssued) {
        if (taskModel.ReIssued == true) {
          taskModelConvertor.ReIssued = AppConstants.yes;
        }else{
          taskModelConvertor.ReIssued = AppConstants.No;
        }
      }

      if (taskModel.Rejected) {
        if (taskModel.Rejected == true) {
          taskModelConvertor.Rejected = AppConstants.yes;
        }else{
          taskModelConvertor.Rejected = AppConstants.No;
        }
      }

      if (taskModel.Resolved) {
        if (taskModel.Resolved == true) {
          taskModelConvertor.Resolved = AppConstants.yes;
        }else{
          taskModelConvertor.Resolved = AppConstants.No;
        }
      }

      if (taskModel.Solved) {
        if (taskModel.Solved == true) {
          taskModelConvertor.Solved = AppConstants.yes;
        }else{
          taskModelConvertor.Solved = AppConstants.No;
        }
      }




      if (taskModel.Started) {
        if (taskModel.Started == true) {
          taskModelConvertor.Started = AppConstants.yes;
        }else{
          taskModelConvertor.Started = AppConstants.No;
        }
      }

      if (taskModel.UnResolved) {
        if (taskModel.UnResolved == true) {
          taskModelConvertor.UnResolved = AppConstants.yes;
        }else{
          taskModelConvertor.UnResolved = AppConstants.No;
        }
      }

      if (taskModel.UnSolved) {
        if (taskModel.UnSolved == true) {
          taskModelConvertor.UnSolved = AppConstants.yes;
        }else{
          taskModelConvertor.UnSolved = AppConstants.No;
        }
      }

      if (taskModel.Accepted) {
        if (taskModel.Accepted == true) {
          taskModelConvertor.Accepted = AppConstants.yes;
        }else{
          taskModelConvertor.Accepted = AppConstants.No;
        }
      }

      return taskModelConvertor;

    
  }



  // async onIssueSubmit() {
  //   this.isIssueSubmitted = false;
  //   this.issueModel.TaskBy = this.UserId;
  //   this.issueModel.From = this.UserName;
  //   if (this.userChipArray.length > 0) {
  //     this.issueModel.TaskTo = this.userChipArray[0].UserId;
  //     this.issueModel.To = this.userChipArray[0].UserName;
  //   } else {
  //     if (this.platform1) {
  //       this.onClickPlatform1(this.platform1);
  //       if (this.userChipArray.length > 0) {
  //         this.issueModel.TaskTo = this.userChipArray[0].UserId;
  //         this.issueModel.To = this.userChipArray[0].UserName;
  //       }
  //     }
  //   }
  //   this.issueModel.Img = new Array();
  //   for (var i = 0; i < this.images.length; i++) {
  //     console.log("The docs are ", this.images[i].receivedDocid);
  //     if (this.images[i].isUploaded && this.images[i].receivedDocid) {
  //       this.issueModel.Img.push(this.images[i].receivedDocid);
  //     }
  //   }

  //   console.log(this.issueModel);
  //   let response = await this.networkService.performRequest(AppConstants.method.post, AppConstants.postIssue, this.issueModel);
  //   console.log("POST ISSUE ", response);
  //   if (response[AppConstants.res.status] == 1) {
  //     this.issueModel = new IssueMainModel();
  //     this.removeFirstElement();
  //     this.filesStatus = "No files Attached";
  //     this.images = [];
  //     this.platform1 = undefined;
  //     setTimeout(() => {
  //       this.onIntoClicked();
  //     }, 400);

  //     this.resetAll('onSubmit');
  //     let responseTasks = await this.networkService.performRequest(AppConstants.method.post, AppConstants.filterIssues, this.filterClass);
  //     if (responseTasks[AppConstants.res.status] == 1) {
  //       let res2 = responseTasks[AppConstants.res.data];
  //       setTimeout(() => {
  //         this.taskModelConvertor = [];
  //         this.taskModel = [];

  //       }, 700);
  //       setTimeout(() => {
  //         this.isIssueSubmitted = true;
  //         this.toast = "Issue Submitted Successfully"
  //       }, 800);
  //       setTimeout(async () => {
  //         this.taskModelConvertor = res2;
  //         await this.convertorToTaskModel(this.taskModelConvertor);
  //       }, 1500);
  //     }

  //   }
  // }

  // async onClickAccept(task: IssueMainModel) {
  //   task.Accepted = true;
  //   task.DevId = this.UserId;
  //   task.DevName = this.UserName;

  //   let convertedModel = await this.TaskModelToConvertor(task);
  //   let update = await this.networkService.performRequest(AppConstants.method.post, AppConstants.updateIssue, convertedModel);
  //   console.log(update);
  //   if (update[AppConstants.res.status] == 1) {
  //     setTimeout(() => {
  //       this.toast = "Issue Accepted Sucessfully";
  //       this.isIssueSubmitted = true;
  //     }, 100);

  //   } else {
  //     task.Accepted = false;
  //     task.DevId = null;
  //     task.DevName = null;
  //   }

  // }

  // async openDialogRejected(task: IssueMainModel) {
  //   const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
  //     width: '45%',
  //     data: { name: task.TaskTitle, remark: "", title: "Reject", description: "Rejection" }
  //   });

  //   dialogRef.afterClosed().subscribe(async result => {
  //     console.log('The dialog was closed ', task.TaskTitle);
  //     if (result) {
  //       task.Rejected = true;
  //       task.DevId = this.UserId;
  //       task.DevName = this.UserName;
  //       task.DevRemark = result;

  //       let convertedModel = await this.TaskModelToConvertor(task);
  //       let update = await this.networkService.performRequest(AppConstants.method.post, AppConstants.updateIssue, convertedModel);
  //       console.log(update);
  //       if (update[AppConstants.res.status] == 1) {
  //         setTimeout(() => {
  //           this.toast = "Issue Rejected Sucessfully";
  //           this.isIssueSubmitted = true;
  //         }, 100);

  //       } else {
  //         task.Rejected = false;
  //         task.DevId = null;
  //         task.DevName = null;
  //         task.DevRemark = null;
  //       }
  //     }

  //   });
  // }

  // async onClickSolved(task: IssueMainModel) {
  //   task.Solved = true;
    
  //   let convertedModel = await this.TaskModelToConvertor(task);
  //   let update = await this.networkService.performRequest(AppConstants.method.post, AppConstants.updateIssue, convertedModel);
  //   console.log(update);
  //   if (update[AppConstants.res.status] == 1) {
  //     setTimeout(() => {
  //       this.toast = "Issue Solved Sucessfully";
  //       this.isIssueSubmitted = true;
  //     }, 100);

  //   } else {
  //     task.Solved = false;
  //   }

  // }

  // async onClickResolved(task: IssueMainModel) {
  //   task.Resolved = true;

  //   let convertedModel = await this.TaskModelToConvertor(task);
  //   let update = await this.networkService.performRequest(AppConstants.method.post, AppConstants.updateIssue, convertedModel);
  //   console.log(update);
  //   if (update[AppConstants.res.status] == 1) {
  //     setTimeout(() => {
  //       this.toast = "Issue Resolved Sucessfully";
  //       this.isIssueSubmitted = true;
  //     }, 100);

  //   } else {
  //     task.Resolved = false;
  //   }

  // }


  // async openDialogUnsolved(task: IssueMainModel) {
  //   const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
  //     width: '45%',
  //     data: { name: task.TaskTitle, remark: "", title: "Un Solved", description: "Un solved" }
  //   });

  //   dialogRef.afterClosed().subscribe(async result => {
  //     console.log('The dialog was closed ', task.TaskTitle);
  //     if (result) {
  //       task.UnSolved = true;
  //       task.DevRemark = result;

  //       let convertedModel = await this.TaskModelToConvertor(task);
  //       let update = await this.networkService.performRequest(AppConstants.method.post, AppConstants.updateIssue, convertedModel);
  //       console.log(update);
  //       if (update[AppConstants.res.status] == 1) {
  //         setTimeout(() => {
  //           this.toast = "Issue Unsolved Sucessfully";
  //           this.isIssueSubmitted = true;
  //         }, 100);

  //       } else {
  //         task.UnSolved = false;
  //         task.DevRemark = null;
  //       }
  //     }

  //   });
  // }

  // async openDialogUnResolved(task: IssueMainModel) {
  //   const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
  //     width: '45%',
  //     data: { name: task.TaskTitle, remark: "", title: "Un Resolved", description: "Un Resolved" }
  //   });

  //   dialogRef.afterClosed().subscribe(async result => {
  //     console.log('The dialog was closed ', task.TaskTitle);
  //     if (result) {
  //       task.UnResolved = true;
  //       task.TesterRemark = result;

  //       let convertedModel = await this.TaskModelToConvertor(task);
  //       let update = await this.networkService.performRequest(AppConstants.method.post, AppConstants.updateIssue, convertedModel);
  //       console.log(update);
  //       if (update[AppConstants.res.status] == 1) {
  //         setTimeout(() => {
  //           this.toast = "Issue Un Resolved Sucessfully";
  //           this.isIssueSubmitted = true;
  //         }, 100);

  //       } else {
  //         task.UnResolved = false;
  //         task.TesterRemark = null;
  //       }
  //     }

  //   });
  // }

  async sortAndFilterData() {
    if (this.MainIssueName) {
      if (this.MainIssueName.trim() == '') {
        this.filterClass.TaskBy = null;
      } else if (this.MainIssueName == 'All') {
        this.filterClass.TaskBy = null;
      }
    }

    if (this.MainAssignName) {
      if (this.MainAssignName.trim() == '') {
        this.filterClass.TaskTo = null;
      } else if (this.MainAssignName == 'All') {
        this.filterClass.TaskTo = null;
      }
    }

    this.callFilteredIssues();
  }


  canIShowDeveloper(task: IssueMainModel){
    if (!task.Accepted && !task.Rejected) {
      if (task.TaskTo == this.UserId || task.TaskTo == this.UserDes) {
        return true;
      } else {
        return false;
      }
    }else{
      return true
    }
  }

  canIShowTester(task: IssueMainModel){
    if (task.Accepted || task.Rejected){
      if (task.Solved || task.UnSolved || task.Rejected){
        if (task.UnResolved || task.Resolved) {
          return true;
        }else{
          if (task.TaskBy == this.UserId) {
            return true;
          }else{
            return false;
          }
        }
      }
    }
  }

}
