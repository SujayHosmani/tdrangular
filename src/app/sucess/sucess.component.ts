import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SucessService } from '../Helpers/SucessService';

@Component({
  selector: 'app-sucess',
  templateUrl: './sucess.component.html',
  styleUrls: ['./sucess.component.css']
})
export class SucessComponent {

  title = 'Angular-Interceptor';
  datas:string;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private sucess: SucessService,public router: Router,) {
    this.datas = this.data;
  }

 

  closeTheDialog(){
    if (this.datas == "Registered sucessfully"){
      this.sucess.closeTheDialog();
      // this.router.navigate(['project']);
    }else{
      this.sucess.closeTheDialog();
    }
  }

}
