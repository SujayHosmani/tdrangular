import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import * as $ from 'jquery';
import {NetworkService} from './Helpers/NetworService';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {RouteReuseStrategy} from '@angular/router';
import {CustomReuseStrategy} from './Helpers/CustomReuseStrategy';
import { MatDialogModule } from '@angular/material/dialog';
import { ProjectsComponent } from './projects/projects.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DiscussionComponent } from './discussion/discussion.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorService } from './Helpers/ErrorService';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule, MAT_CHIPS_DEFAULT_OPTIONS } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { DragDropModule } from '@angular/cdk/drag-drop'
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSelectModule } from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { SucessService } from './Helpers/SucessService';
import { ProgressComponent } from './progress/progress.component';
import { ErrorComponent } from './error/error.component';
import { ProgressService } from './Helpers/ProgressService';
import { SucessComponent } from './sucess/sucess.component';
import { DialogOverviewExampleDialog } from './Dialog/dialog-overview-example-dialog';
import { ChatComponent } from './chat/chat.component';
import { MainHomeComponent } from './main-home/main-home.component';
import { DummyComponent } from './dummy/dummy.component';
import { TesterComponent } from './tester/tester.component';
import { DeveloperComponent } from './developer/developer.component';
import { AdminComponent } from './admin/admin.component';
import { ActivityComponent } from './activity/activity.component';
import { TempComponent } from './temp/temp.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { ChatService } from './chat/chat.service';
import { MyTaskComponent } from './my-task/my-task.component';
import { ProfileComponent } from './profile/profile.component';
import { AnalysisComponent } from './analysis/analysis.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProjectsComponent,
    LoginComponent,
    RegisterComponent,
    DiscussionComponent,
    ProgressComponent,
    ErrorComponent,
    SucessComponent,
    DialogOverviewExampleDialog,
    ChatComponent,
    MainHomeComponent,
    DummyComponent,
    TesterComponent,
    DeveloperComponent,
    AdminComponent,
    ActivityComponent,
    TempComponent,
    DashBoardComponent,
    MyTaskComponent,
    ProfileComponent,
    AnalysisComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatInputModule,
    MatDividerModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatChipsModule,
    MatBadgeModule,
    NgbModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatTabsModule,
    MatNativeDateModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatSidenavModule,
    DragDropModule,
    MatSelectModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    { provide: RouteReuseStrategy, useClass: CustomReuseStrategy },
    {provide: MAT_CHIPS_DEFAULT_OPTIONS,useValue: {separatorKeyCodes: [13, 188]}},
    NetworkService, ErrorService, ProgressService,SucessService, MatDatepickerModule, ChatService,
    MatNativeDateModule ],
  bootstrap: [AppComponent]
})
export class AppModule { }
